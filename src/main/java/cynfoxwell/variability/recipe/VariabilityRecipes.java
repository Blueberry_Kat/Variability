package cynfoxwell.variability.recipe;

import com.elytradev.iridescent.module.stoned.ModuleStoned;
import com.google.common.collect.Lists;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.config.ConfigHandler;
import cynfoxwell.variability.item.ItemChromaticDye;
import cynfoxwell.variability.item.ItemPreciseDye;
import cynfoxwell.variability.item.ModItems;
import cynfoxwell.variability.item.dyable.IDyable;
import cynfoxwell.variability.util.ColorList;
import net.minecraft.init.Blocks;
import net.minecraft.init.Items;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.oredict.ShapedOreRecipe;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Level;
import pl.asie.balancedclaytools.ModBalancedClayTools;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.io.File;
import java.util.List;

@Mod.EventBusSubscriber(modid = Variability.MODID)
public class VariabilityRecipes {
    @SubscribeEvent
    public static void onRegisterRecipes(RegistryEvent.Register<IRecipe> event) {
        IForgeRegistry<IRecipe> registry = event.getRegistry();

        Variability.LOGGER.log(Level.INFO, "Registering Recipes");

        if (ConfigHandler.enableBBag)
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.baubleBag),
                    "ssn",
                    "lcl",
                    "lll",
                    's', "string",
                    'n', "nuggetGold",
                    'l', "leather",
                    'c', "chestWood"
            ).setRegistryName("bauble_bag"));


        if (ConfigHandler.enableCosmetics) {
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.chromaticDye),
                    "roy",
                    "gwb",
                    "ivp",
                    'r', "dyeRed",
                    'o', "dyeOrange",
                    'y', "dyeYellow",
                    'g', "dyeLime",
                    'b', "dyeBlue",
                    'i', "dyeLightBlue",
                    'v', "dyePurple",
                    'p', "dyePink",
                    'w', "dyeWhite"
            ).setRegistryName("chromatic_dye"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.pastelDye),
                    "ccc",
                    "cwc",
                    "ccc",
                    'c', new ItemStack(ModItems.chromaticDye),
                    'w', "dyeWhite"
            ).setRegistryName("pastel_dye"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.chromaticDye),
                    "ccc",
                    "cwc",
                    "ccc",
                    'c', new ItemStack(ModItems.pastelDye),
                    'w', "dyeBlack"
            ).setRegistryName("pastel_to_chromatic"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.cloth),
                    "ws",
                    "sw",
                    's', "string",
                    'w', new ItemStack(Blocks.WOOL,1, OreDictionary.WILDCARD_VALUE)
            ).setRegistryName("cloth"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.scarf),
                    "c c",
                    "sss",
                    'c', new ItemStack(ModItems.cloth),
                    's', "string"
            ).setRegistryName("scarf"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.gloves),
                    " l ",
                    "lll",
                    "ll ",
                    'l', "leather"
            ).setRegistryName("gloves"));

            registry.register(new ShapelessOreRecipe(null, new ItemStack(ModItems.preciseDye),
                    new ItemStack(ModItems.chromaticDye),
                    "dyeWhite"
            ).setRegistryName("precise_dye"));

            //Dying
            registry.register(new DyingRecipe(null, new ItemStack(ModItems.scarf),
                    new ItemStack(ModItems.scarf),
                    "dye"
            ).setRegistryName("scarf_dying"));

            registry.register(new DyingRecipe(null, new ItemStack(ModItems.gloves),
                    new ItemStack(ModItems.gloves),
                    "dye"
            ).setRegistryName("gloves_dying"));

            //Chromatic
            registry.register(new ChromaticDyingRecipe(null, new ItemStack(ModItems.scarf),
                    new ItemStack(ModItems.scarf),
                    new ItemStack(ModItems.chromaticDye)
            ).setRegistryName("scarf_chromatic"));

            registry.register(new ChromaticDyingRecipe(null, new ItemStack(ModItems.gloves),
                    new ItemStack(ModItems.gloves),
                    new ItemStack(ModItems.chromaticDye)
            ).setRegistryName("gloves_chromatic"));

            //Precise
            registry.register(new PreciseDyingRecipe(null, new ItemStack(ModItems.scarf),
                    new ItemStack(ModItems.scarf),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("scarf_precise"));

            registry.register(new PreciseDyingRecipe(null, new ItemStack(ModItems.gloves),
                    new ItemStack(ModItems.gloves),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("gloves_precise"));

            //Thighhighs
            for(int i = 0; i < 16; i++) {
                registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.programmingSocks, 1, i),
                        "c c",
                        "cdc",
                        "c c",
                        'c', new ItemStack(ModItems.cloth),
                        'd', "dye" + ColorList.values()[i].oreName
                ).setRegistryName("thighhighs_" + ColorList.values()[i].regName));

                registry.register(new DyingRecipe(null, new ItemStack(ModItems.programmingSocks,1,i),
                        new ItemStack(ModItems.programmingSocks,1,i),
                        "dye"
                ).setRegistryName("thighhighs_dying_"+ColorList.values()[i].regName));

                registry.register(new ChromaticDyingRecipe(null, new ItemStack(ModItems.programmingSocks,1,i),
                        new ItemStack(ModItems.programmingSocks,1,i),
                        new ItemStack(ModItems.chromaticDye)
                ).setRegistryName("thighhighs_chromatic_"+ColorList.values()[i].regName));

                registry.register(new PreciseDyingRecipe(null, new ItemStack(ModItems.programmingSocks,1,i),
                        new ItemStack(ModItems.programmingSocks,1,i),
                        new ItemStack(ModItems.preciseDye)
                ).setRegistryName("thighhighs_precise_"+ColorList.values()[i].regName));
            }

            //Misc
            registry.register(new PreciseDyingRecipeNoCheck(null, new ItemStack(Items.LEATHER_HELMET),
                    new ItemStack(Items.LEATHER_HELMET),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("leather_helmet_precise"));
            registry.register(new PreciseDyingRecipeNoCheck(null, new ItemStack(Items.LEATHER_CHESTPLATE),
                    new ItemStack(Items.LEATHER_CHESTPLATE),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("leather_chest_precise"));
            registry.register(new PreciseDyingRecipeNoCheck(null, new ItemStack(Items.LEATHER_LEGGINGS),
                    new ItemStack(Items.LEATHER_LEGGINGS),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("leather_leggings_precise"));
            registry.register(new PreciseDyingRecipeNoCheck(null, new ItemStack(Items.LEATHER_BOOTS),
                    new ItemStack(Items.LEATHER_BOOTS),
                    new ItemStack(ModItems.preciseDye)
            ).setRegistryName("leather_boots_precise"));

        }

        if (ConfigHandler.enablePaxels) {
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.woodPaxel),
                    "123",
                    " s ",
                    " s ",
                    's', "stickWood",
                    '1', new ItemStack(Items.WOODEN_AXE),
                    '2', new ItemStack(Items.WOODEN_SHOVEL),
                    '3', new ItemStack(Items.WOODEN_PICKAXE)
            ).setRegistryName("wood_paxel"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.stonePaxel),
                    "123",
                    " s ",
                    " s ",
                    's', "stickWood",
                    '1', new ItemStack(Items.STONE_AXE),
                    '2', new ItemStack(Items.STONE_SHOVEL),
                    '3', new ItemStack(Items.STONE_PICKAXE)
            ).setRegistryName("stone_paxel"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.ironPaxel),
                    "123",
                    " s ",
                    " s ",
                    's', "stickWood",
                    '1', new ItemStack(Items.IRON_AXE),
                    '2', new ItemStack(Items.IRON_SHOVEL),
                    '3', new ItemStack(Items.IRON_PICKAXE)
            ).setRegistryName("iron_paxel"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.goldPaxel),
                    "123",
                    " s ",
                    " s ",
                    's', "stickWood",
                    '1', new ItemStack(Items.GOLDEN_AXE),
                    '2', new ItemStack(Items.GOLDEN_SHOVEL),
                    '3', new ItemStack(Items.GOLDEN_PICKAXE)
            ).setRegistryName("gold_paxel"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.diamondPaxel),
                    "123",
                    " s ",
                    " s ",
                    's', "stickWood",
                    '1', new ItemStack(Items.DIAMOND_AXE),
                    '2', new ItemStack(Items.DIAMOND_SHOVEL),
                    '3', new ItemStack(Items.DIAMOND_PICKAXE)
            ).setRegistryName("diamond_paxel"));

            if(Loader.isModLoaded("balancedclaytools")) {
                registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.clayPaxel),
                        "123",
                        " s ",
                        " s ",
                        's', "stickWood",
                        '1', new ItemStack(ModBalancedClayTools.clayAxe),
                        '2', new ItemStack(ModBalancedClayTools.clayShovel),
                        '3', new ItemStack(ModBalancedClayTools.clayPickaxe)
                ).setRegistryName("clay_paxel"));
            }

            if(Loader.isModLoaded("iridescent")) {
                Configuration c = new Configuration(new File("config/iridescent.cfg"));

                if (c.getBoolean("stoned", "modules", true, "")) {
                    registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.cobblePaxel),
                            "123",
                            " s ",
                            " s ",
                            's', "stickWood",
                            '1', new ItemStack(ModuleStoned.COBBLESTONE_AXE),
                            '2', new ItemStack(ModuleStoned.COBBLESTONE_SHOVEL),
                            '3', new ItemStack(ModuleStoned.COBBLESTONE_PICKAXE)
                    ).setRegistryName("cobblestone_paxel"));

                    registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.flintPaxel),
                            "123",
                            " s ",
                            " s ",
                            's', "stickWood",
                            '1', new ItemStack(ModuleStoned.FLINT_AXE),
                            '2', new ItemStack(ModuleStoned.FLINT_SHOVEL),
                            '3', new ItemStack(ModuleStoned.FLINT_PICKAXE)
                    ).setRegistryName("flint_paxel"));

                    registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.granitePaxel),
                            "123",
                            " s ",
                            " s ",
                            's', "stickWood",
                            '1', new ItemStack(ModuleStoned.GRANITE_AXE),
                            '2', new ItemStack(ModuleStoned.GRANITE_SHOVEL),
                            '3', new ItemStack(ModuleStoned.GRANITE_PICKAXE)
                    ).setRegistryName("granite_paxel"));

                    registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.andesitePaxel),
                            "123",
                            " s ",
                            " s ",
                            's', "stickWood",
                            '1', new ItemStack(ModuleStoned.ANDESITE_AXE),
                            '2', new ItemStack(ModuleStoned.ANDESITE_SHOVEL),
                            '3', new ItemStack(ModuleStoned.ANDESITE_PICKAXE)
                    ).setRegistryName("andesite_paxel"));

                    registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.dioritePaxel),
                            "123",
                            " s ",
                            " s ",
                            's', "stickWood",
                            '1', new ItemStack(ModuleStoned.DIORITE_AXE),
                            '2', new ItemStack(ModuleStoned.DIORITE_SHOVEL),
                            '3', new ItemStack(ModuleStoned.DIORITE_PICKAXE)
                    ).setRegistryName("diorite_paxel"));
                }
            }
        }

        if(ConfigHandler.enableAgriculture){
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canWood),
                    "wm ",
                    "wbw",
                    " w ",
                    'w', "logWood",
                    'b', new ItemStack(Items.BOWL),
                    'm', "dyeWhite"
            ).setRegistryName("watering_can_wood"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canStone),
                    "s  ",
                    "scs",
                    " s ",
                    's', "stone",
                    'c', new ItemStack(ModItems.canWood)
            ).setRegistryName("watering_can_stone"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canIron),
                    "i  ",
                    "ici",
                    " i ",
                    'i', "ingotIron",
                    'c', new ItemStack(ModItems.canStone)
            ).setRegistryName("watering_can_iron"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canGold),
                    "g  ",
                    "gcg",
                    " g ",
                    'g', "ingotGold",
                    'c', new ItemStack(ModItems.canIron)
            ).setRegistryName("watering_can_gold"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canDiamond),
                    "d  ",
                    "dcd",
                    " d ",
                    'd', "gemDiamond",
                    'c', new ItemStack(ModItems.canGold)
            ).setRegistryName("watering_can_diamond"));
            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canEmerald),
                    "eb ",
                    "ece",
                    " e ",
                    'e', "gemEmerald",
                    'b', new ItemStack(Items.CLAY_BALL),
                    'c', new ItemStack(ModItems.canDiamond)
            ).setRegistryName("watering_can_emerald"));

            registry.register(new ShapedOreRecipe(null, new ItemStack(ModItems.canIron),
                    "im ",
                    "ibi",
                    " i ",
                    'i', "ingotIron",
                    'b', new ItemStack(Items.WATER_BUCKET),
                    'm', "dyeWhite"
            ).setRegistryName("watering_can_iron_alt"));
        }
    }

    private static class DyingRecipe extends ShapelessOreRecipe {
        private ItemStack output;

        public DyingRecipe(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            this.output = result;
        }

        @Override
        public boolean matches(InventoryCrafting inv, World worldIn)
        {
            ItemStack itemstack = ItemStack.EMPTY;
            List<ItemStack> list = Lists.<ItemStack>newArrayList();

            for (int i = 0; i < inv.getSizeInventory(); ++i)
            {
                ItemStack itemstack1 = inv.getStackInSlot(i);

                if (!itemstack1.isEmpty())
                {
                    if (itemstack1.getItem() instanceof IDyable)
                    {
                        IDyable itemdyable = (IDyable)itemstack1.getItem();

                        if ( !itemstack.isEmpty())
                        {
                            return false;
                        }

                        itemstack = itemstack1;
                    }
                    else
                    {
                        if (!net.minecraftforge.oredict.DyeUtils.isDye(itemstack1))
                        {
                            return false;
                        }

                        list.add(itemstack1);
                    }
                }
            }

            return !itemstack.isEmpty() && !list.isEmpty();
        }

        @Override
        public ItemStack getCraftingResult(InventoryCrafting inv)
        {
            ItemStack itemstack = ItemStack.EMPTY;
            int[] aint = new int[3];
            int i = 0;
            int j = 0;
            IDyable itemdyable = null;

            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty())
                {
                    if (itemstack1.getItem() instanceof IDyable)
                    {
                        itemdyable = (IDyable)itemstack1.getItem();

                        if (!itemstack.isEmpty())
                        {
                            return ItemStack.EMPTY;
                        }

                        itemstack = itemstack1.copy();
                        itemstack.setCount(1);

                        if (itemdyable.hasColor(itemstack1))
                        {
                            int l = itemdyable.getColor(itemstack);
                            float f = (float)(l >> 16 & 255) / 255.0F;
                            float f1 = (float)(l >> 8 & 255) / 255.0F;
                            float f2 = (float)(l & 255) / 255.0F;
                            i = (int)((float)i + Math.max(f, Math.max(f1, f2)) * 255.0F);
                            aint[0] = (int)((float)aint[0] + f * 255.0F);
                            aint[1] = (int)((float)aint[1] + f1 * 255.0F);
                            aint[2] = (int)((float)aint[2] + f2 * 255.0F);
                            ++j;
                        }
                    }
                    else
                    {
                        if (!net.minecraftforge.oredict.DyeUtils.isDye(itemstack1))
                        {
                            return ItemStack.EMPTY;
                        }

                        float[] afloat = net.minecraftforge.oredict.DyeUtils.colorFromStack(itemstack1).get().getColorComponentValues();
                        int l1 = (int)(afloat[0] * 255.0F);
                        int i2 = (int)(afloat[1] * 255.0F);
                        int j2 = (int)(afloat[2] * 255.0F);
                        i += Math.max(l1, Math.max(i2, j2));
                        aint[0] += l1;
                        aint[1] += i2;
                        aint[2] += j2;
                        ++j;
                    }
                }
            }

            if (itemdyable == null)
            {
                return ItemStack.EMPTY;
            }
            else
            {
                int i1 = aint[0] / j;
                int j1 = aint[1] / j;
                int k1 = aint[2] / j;
                float f3 = (float)i / (float)j;
                float f4 = (float)Math.max(i1, Math.max(j1, k1));
                i1 = (int)((float)i1 * f3 / f4);
                j1 = (int)((float)j1 * f3 / f4);
                k1 = (int)((float)k1 * f3 / f4);
                int k2 = (i1 << 8) + j1;
                k2 = (k2 << 8) + k1;
                itemdyable.setColor(itemstack, k2);
                return itemstack;
            }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }

        @Override
        public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv)
        {
            NonNullList<ItemStack> nonnulllist = NonNullList.<ItemStack>withSize(inv.getSizeInventory(), ItemStack.EMPTY);

            for (int i = 0; i < nonnulllist.size(); ++i)
            {
                ItemStack itemstack = inv.getStackInSlot(i);
                nonnulllist.set(i, net.minecraftforge.common.ForgeHooks.getContainerItem(itemstack));
            }

            return nonnulllist;
        }

        @Override
        public boolean isDynamic()
        {
            return true;
        }

        @Override
        public boolean canFit(int width, int height)
        {
            return width * height >= 2;
        }
    }

    public static class ChromaticDyingRecipe extends ShapelessOreRecipe {
        private ItemStack output;

        public ChromaticDyingRecipe(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        @Nonnull
        public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
            ItemStack itemstack = ItemStack.EMPTY;
            IDyable itemdyable = null;
            ItemChromaticDye itemdye = null;

            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty()) {
                    if(itemstack1.getItem() instanceof ItemChromaticDye){
                        itemdye = (ItemChromaticDye)itemstack1.getItem();
                    }
                    else if (itemstack1.getItem() instanceof IDyable)
                    {
                        itemdyable = (IDyable)itemstack1.getItem();

                        if (!itemstack.isEmpty())
                        {
                            return ItemStack.EMPTY;
                        }

                        itemstack = itemstack1.copy();
                        itemstack.setCount(1);
                    }
                    else {
                        return ItemStack.EMPTY;
                    }
                }
            }

            if (itemdyable == null)
            {
                return ItemStack.EMPTY;
            }
            else if (itemdye == null)
            {
                return ItemStack.EMPTY;
            }
            else
            {
                NBTTagCompound nbttagcompound = itemstack.getTagCompound();

                if (nbttagcompound == null)
                {
                    nbttagcompound = new NBTTagCompound();
                    itemstack.setTagCompound(nbttagcompound);
                }

                nbttagcompound.setBoolean("isChromatic",true);
                return itemstack;
            }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }
    }

    private static class PreciseDyingRecipe extends ShapelessOreRecipe {
        private ItemStack output;

        public PreciseDyingRecipe(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        @Nonnull
        public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
            ItemStack itemstack = ItemStack.EMPTY;
            IDyable itemdyable = null;
            int col = 0xFFFFFF;

            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty())
                {
                    if (itemstack1.getItem() instanceof ItemPreciseDye) {
                        col = ((ItemPreciseDye)itemstack1.getItem()).getColor(itemstack1);
                    }
                    else if (itemstack1.getItem() instanceof IDyable)
                    {
                        itemdyable = (IDyable)itemstack1.getItem();

                        if (!itemstack.isEmpty())
                        {
                            return ItemStack.EMPTY;
                        }

                        itemstack = itemstack1.copy();
                        itemstack.setCount(1);
                    }
                    else {
                        return ItemStack.EMPTY;
                    }
                }
            }

            if (itemdyable == null)
            {
                return ItemStack.EMPTY;
            }
            else
            {
                itemdyable.setColor(itemstack, col);
                return itemstack;
            }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }
    }

    public static class PreciseDyingRecipeNoCheck extends ShapelessOreRecipe {
        private ItemStack output;

        public PreciseDyingRecipeNoCheck(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        @Nonnull
        public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
            ItemStack itemstack = ItemStack.EMPTY;
            Item itemdyable = null;
            int col = 0xFFFFFF;

            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty())
                {
                    if (itemstack1.getItem() instanceof ItemPreciseDye) {
                        col = ((ItemPreciseDye)itemstack1.getItem()).getColor(itemstack1);
                    }
                    else {
                        itemdyable = itemstack1.getItem();

                        if (!itemstack.isEmpty())
                        {
                            return ItemStack.EMPTY;
                        }

                        itemstack = itemstack1.copy();
                        itemstack.setCount(1);
                    }
                }
            }

            if (itemdyable == null)
            {
                return ItemStack.EMPTY;
            }
            else
            {
                NBTTagCompound nbttagcompound = itemstack.getTagCompound();

                if (nbttagcompound == null)
                {
                    nbttagcompound = new NBTTagCompound();
                    itemstack.setTagCompound(nbttagcompound);
                }

                NBTTagCompound nbttagcompound1 = nbttagcompound.getCompoundTag("display");

                if (!nbttagcompound.hasKey("display", 10))
                {
                    nbttagcompound.setTag("display", nbttagcompound1);
                }

                nbttagcompound1.setInteger("color", col);
                return itemstack;
            }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }
    }

    public static class DyingRecipeNoCheck extends ShapelessOreRecipe {
        private ItemStack output;

        public DyingRecipeNoCheck(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        @Nonnull
        public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
            ItemStack itemstack = this.output.copy();
            itemstack.setCount(1);
            int[] aint = new int[3];
            int i = 0;
            int j = 0;

            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty())
                {
                    if (net.minecraftforge.oredict.DyeUtils.isDye(itemstack1)) {
                        float[] afloat = net.minecraftforge.oredict.DyeUtils.colorFromStack(itemstack1).get().getColorComponentValues();
                        int l1 = (int)(afloat[0] * 255.0F);
                        int i2 = (int)(afloat[1] * 255.0F);
                        int j2 = (int)(afloat[2] * 255.0F);
                        i += Math.max(l1, Math.max(i2, j2));
                        aint[0] += l1;
                        aint[1] += i2;
                        aint[2] += j2;
                        ++j;
                    }
                    else {
                        NBTTagCompound nbttagcompound = itemstack1.getTagCompound();

                        if (nbttagcompound != null && nbttagcompound.hasKey("display", 10) ? nbttagcompound.getCompoundTag("display").hasKey("color", 3) : false)
                        {
                            int l = 0xFFFFFF;
                            NBTTagCompound nbttagcompound1 = nbttagcompound.getCompoundTag("display");

                            if (nbttagcompound1 != null && nbttagcompound1.hasKey("color", 3))
                            {
                                l = nbttagcompound1.getInteger("color");
                            }
                            float f = (float)(l >> 16 & 255) / 255.0F;
                            float f1 = (float)(l >> 8 & 255) / 255.0F;
                            float f2 = (float)(l & 255) / 255.0F;
                            i = (int)((float)i + Math.max(f, Math.max(f1, f2)) * 255.0F);
                            aint[0] = (int)((float)aint[0] + f * 255.0F);
                            aint[1] = (int)((float)aint[1] + f1 * 255.0F);
                            aint[2] = (int)((float)aint[2] + f2 * 255.0F);
                            ++j;
                        }
                    }
                }
            }

            if (itemstack == null)
            {
                return ItemStack.EMPTY;
            }
            else
            {
                NBTTagCompound nbttagcompound = itemstack.getTagCompound();

                if (nbttagcompound == null)
                {
                    nbttagcompound = new NBTTagCompound();
                    itemstack.setTagCompound(nbttagcompound);
                }

                NBTTagCompound nbttagcompound1 = nbttagcompound.getCompoundTag("display");

                if (!nbttagcompound.hasKey("display", 10))
                {
                    nbttagcompound.setTag("display", nbttagcompound1);
                }

                int i1 = aint[0] / j;
                int j1 = aint[1] / j;
                int k1 = aint[2] / j;
                float f3 = (float)i / (float)j;
                float f4 = (float)Math.max(i1, Math.max(j1, k1));
                i1 = (int)((float)i1 * f3 / f4);
                j1 = (int)((float)j1 * f3 / f4);
                k1 = (int)((float)k1 * f3 / f4);
                int k2 = (i1 << 8) + j1;
                k2 = (k2 << 8) + k1;

                nbttagcompound1.setInteger("color", k2);
                return itemstack;
            }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }
    }
}
