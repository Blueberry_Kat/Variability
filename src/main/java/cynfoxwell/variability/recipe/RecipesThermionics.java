package cynfoxwell.variability.recipe;

import com.elytradev.thermionics.data.MachineRecipes;
import com.elytradev.thermionics.data.SergerRecipe;
import com.elytradev.thermionics.data.WildcardNBTIngredient;
import com.elytradev.thermionics.item.EnumIngredient;
import com.elytradev.thermionics.item.ItemFabricSquare;
import com.elytradev.thermionics.item.ThermionicsItems;
import com.elytradev.thermionics.repackage.com.elytradev.concrete.recipe.ItemIngredient;
import com.elytradev.thermionics.repackage.com.elytradev.concrete.recipe.impl.ShapedInventoryRecipe;
import com.google.common.collect.Lists;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.item.ModItems;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraft.world.World;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Level;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class RecipesThermionics {
    @SubscribeEvent
    public static void onRegisterRecipes(RegistryEvent.Register<IRecipe> event) {
        IForgeRegistry<IRecipe> registry = event.getRegistry();

        Variability.LOGGER.log(Level.INFO, "Thermionics found. Registering Thermionics recipes.");

        ItemIngredient ribbon = ItemIngredient.of(new ItemStack(ThermionicsItems.INGREDIENT, 1, EnumIngredient.RIBBON.ordinal()));
        ItemIngredient anyFabric = new WildcardNBTIngredient(ThermionicsItems.FABRIC_SQUARE);

        SergerRecipe varScarf = new SergerRecipe(
                new ShapedInventoryRecipe(
                        new ItemStack(ModItems.scarf),
                        3, 3,
                        3, 2,
                        false,
                        anyFabric, null, anyFabric,
                        ribbon, ribbon, ribbon
                ),
                6,
                10
        );
        MachineRecipes.register(varScarf);

        registry.register(new DyingRecipeFabric(null, new ItemStack(ThermionicsItems.FABRIC_SQUARE),
                new ItemStack(ThermionicsItems.FABRIC_SQUARE),
                "dye"
        ).setRegistryName("fabric_dying"));
    }

    private static class DyingRecipeFabric extends ShapelessOreRecipe {
        private ItemStack output;

        public DyingRecipeFabric(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        public boolean matches(InventoryCrafting inv, World worldIn)
        {
                ItemStack itemstack = ItemStack.EMPTY;
                List<ItemStack> list = Lists.<ItemStack>newArrayList();

                for (int i = 0; i < inv.getSizeInventory(); ++i) {
                    ItemStack itemstack1 = inv.getStackInSlot(i);

                    if (!itemstack1.isEmpty()) {
                        if (itemstack1.getItem() instanceof ItemFabricSquare) {
                            ItemFabricSquare itemdyable = (ItemFabricSquare) itemstack1.getItem();

                            if (!itemstack.isEmpty()) {
                                return false;
                            }

                            itemstack = itemstack1;
                        } else {
                            if (!net.minecraftforge.oredict.DyeUtils.isDye(itemstack1)) {
                                return false;
                            }

                            list.add(itemstack1);
                        }
                    }
                }

                return !itemstack.isEmpty() && !list.isEmpty();
        }

        @Override
        public ItemStack getCraftingResult(InventoryCrafting inv)
        {
                ItemStack itemstack = ItemStack.EMPTY;
                int[] aint = new int[3];
                int i = 0;
                int j = 0;
                ItemFabricSquare itemdyable = null;
                NBTTagCompound itemNBT;

                for (int k = 0; k < inv.getSizeInventory(); ++k) {
                    ItemStack itemstack1 = inv.getStackInSlot(k);


                    if (!itemstack1.isEmpty()) {
                        if (itemstack1.getItem() instanceof ItemFabricSquare) {
                            itemdyable = (ItemFabricSquare) itemstack1.getItem();

                            if (!itemstack.isEmpty()) {
                                return ItemStack.EMPTY;
                            }

                            itemstack = itemstack1.copy();
                            itemstack.setCount(1);

                            itemNBT = itemstack.getTagCompound();
                            if (itemNBT == null) {
                                itemNBT = new NBTTagCompound();
                                itemstack.setTagCompound(itemNBT);
                                itemNBT.setInteger("Color", 0xFFFFFF);
                            }

                            if (itemNBT.hasKey("Color") && itemNBT.getInteger("Color") != 0xFFFFFF) {
                                int l = itemNBT.getInteger("Color");
                                float f = (float) (l >> 16 & 255) / 255.0F;
                                float f1 = (float) (l >> 8 & 255) / 255.0F;
                                float f2 = (float) (l & 255) / 255.0F;
                                i = (int) ((float) i + Math.max(f, Math.max(f1, f2)) * 255.0F);
                                aint[0] = (int) ((float) aint[0] + f * 255.0F);
                                aint[1] = (int) ((float) aint[1] + f1 * 255.0F);
                                aint[2] = (int) ((float) aint[2] + f2 * 255.0F);
                                ++j;
                            }
                        } else {
                            if (!net.minecraftforge.oredict.DyeUtils.isDye(itemstack1)) {
                                return ItemStack.EMPTY;
                            }

                            float[] afloat = net.minecraftforge.oredict.DyeUtils.colorFromStack(itemstack1).get().getColorComponentValues();
                            int l1 = (int) (afloat[0] * 255.0F);
                            int i2 = (int) (afloat[1] * 255.0F);
                            int j2 = (int) (afloat[2] * 255.0F);
                            i += Math.max(l1, Math.max(i2, j2));
                            aint[0] += l1;
                            aint[1] += i2;
                            aint[2] += j2;
                            ++j;
                        }
                    }
                }

                if (itemdyable == null) {
                    return ItemStack.EMPTY;
                } else {
                    int i1 = aint[0] / j;
                    int j1 = aint[1] / j;
                    int k1 = aint[2] / j;
                    float f3 = (float) i / (float) j;
                    float f4 = (float) Math.max(i1, Math.max(j1, k1));
                    i1 = (int) ((float) i1 * f3 / f4);
                    j1 = (int) ((float) j1 * f3 / f4);
                    k1 = (int) ((float) k1 * f3 / f4);
                    int k2 = (i1 << 8) + j1;
                    k2 = (k2 << 8) + k1;
                    itemNBT = itemstack.getTagCompound();
                    if (itemNBT == null) {
                        itemNBT = new NBTTagCompound();
                        itemstack.setTagCompound(itemNBT);
                    }
                    itemNBT.setInteger("Color", k2);
                    return itemstack;
                }
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }

        @Override
        public NonNullList<ItemStack> getRemainingItems(InventoryCrafting inv)
        {
            NonNullList<ItemStack> nonnulllist = NonNullList.<ItemStack>withSize(inv.getSizeInventory(), ItemStack.EMPTY);

            for (int i = 0; i < nonnulllist.size(); ++i)
            {
                ItemStack itemstack = inv.getStackInSlot(i);
                nonnulllist.set(i, net.minecraftforge.common.ForgeHooks.getContainerItem(itemstack));
            }

            return nonnulllist;
        }

        @Override
        public boolean isDynamic()
        {
            return true;
        }

        @Override
        public boolean canFit(int width, int height)
        {
            return width * height >= 2;
        }
    }
}
