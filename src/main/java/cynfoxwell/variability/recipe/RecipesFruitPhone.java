package cynfoxwell.variability.recipe;

import com.elytradev.fruitphone.item.FruitItems;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.item.ItemPreciseDye;
import cynfoxwell.variability.item.ModItems;
import net.minecraft.inventory.InventoryCrafting;
import net.minecraft.item.ItemStack;
import net.minecraft.item.crafting.IRecipe;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.oredict.ShapelessOreRecipe;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Level;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public class RecipesFruitPhone {
    @SubscribeEvent
    public static void onRegisterRecipes(RegistryEvent.Register<IRecipe> event) {
        IForgeRegistry<IRecipe> registry = event.getRegistry();

        Variability.LOGGER.log(Level.INFO, "Fruit Phone found. Registering dying recipes.");

        registry.register(new FruitDyingRecipePrecise(null, new ItemStack(FruitItems.HANDHELD,1,0),
                new ItemStack(FruitItems.HANDHELD,1,0),
                new ItemStack(ModItems.preciseDye)
        ).setRegistryName("fruitphone_handheld_precise_0"));
        registry.register(new FruitDyingRecipePrecise(null, new ItemStack(FruitItems.HANDHELD,1,1),
                new ItemStack(FruitItems.HANDHELD,1,1),
                new ItemStack(ModItems.preciseDye)
        ).setRegistryName("fruitphone_handheld_precise_1"));

        registry.register(new FruitDyingRecipePrecise(null, new ItemStack(FruitItems.PASSIVE, 1, 0),
                new ItemStack(FruitItems.PASSIVE, 1, 0),
                new ItemStack(ModItems.preciseDye)
        ).setRegistryName("fruitphone_passive_precise_0"));
        registry.register(new FruitDyingRecipePrecise(null, new ItemStack(FruitItems.PASSIVE, 1, 1),
                new ItemStack(FruitItems.PASSIVE, 1, 1),
                new ItemStack(ModItems.preciseDye)
        ).setRegistryName("fruitphone_passive_precise_1"));
    }

    public static class FruitDyingRecipePrecise extends ShapelessOreRecipe {
        private ItemStack output;

        public FruitDyingRecipePrecise(@Nullable ResourceLocation group, @Nonnull ItemStack result, Object... recipe) {
            super(group, result, recipe);
            output = result;
        }

        @Override
        @Nonnull
        public ItemStack getCraftingResult(@Nonnull InventoryCrafting inv) {
            ItemStack itemstack = this.output.copy();
            itemstack.setCount(1);
            int col = 0xFFFFFF;


            for (int k = 0; k < inv.getSizeInventory(); ++k)
            {
                ItemStack itemstack1 = inv.getStackInSlot(k);

                if (!itemstack1.isEmpty())
                {
                    if (itemstack1.getItem() instanceof ItemPreciseDye) {
                        col = ((ItemPreciseDye)itemstack1.getItem()).getColor(itemstack1);
                    }
                }
            }

            NBTTagCompound nbttagcompound = itemstack.getTagCompound();

            if (nbttagcompound == null)
            {
                nbttagcompound = new NBTTagCompound();
                itemstack.setTagCompound(nbttagcompound);
            }

            nbttagcompound.setInteger("fruitphone:color", col);

            return itemstack;
        }

        @Override
        public ItemStack getRecipeOutput()
        {
            return this.output;
        }
    }
}
