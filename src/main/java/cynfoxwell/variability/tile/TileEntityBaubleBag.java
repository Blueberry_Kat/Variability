package cynfoxwell.variability.tile;

import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.nbt.NBTTagList;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.ITickable;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemStackHandler;

public class TileEntityBaubleBag extends TileEntity implements ITickable {
    public final ItemStackHandler inv;
    private String name;

    public TileEntityBaubleBag(int slots, String name){
        this.name = name;
        inv = new TileStackHandler(slots);
    }

    public static void saveSlots(IItemHandler slots, NBTTagCompound compound) {
        if (slots != null && slots.getSlots() > 0) {
            NBTTagList tagList = new NBTTagList();
            for (int i = 0; i < slots.getSlots(); i++) {
                ItemStack slot = slots.getStackInSlot(i);
                NBTTagCompound tagCompound = new NBTTagCompound();
                if (slot != null && slot != ItemStack.EMPTY) {
                    slot.writeToNBT(tagCompound);
                }
                tagList.appendTag(tagCompound);
            }
            compound.setTag("Items", tagList);
        }
    }

    public static void loadSlots(IItemHandlerModifiable slots, NBTTagCompound compound) {
        if (slots != null && slots.getSlots() > 0) {
            NBTTagList tagList = compound.getTagList("Items", 10);
            for (int i = 0; i < slots.getSlots(); i++) {
                NBTTagCompound tagCompound = tagList.getCompoundTagAt(i);
                slots.setStackInSlot(i, tagCompound != null && tagCompound.hasKey("id") ? new ItemStack(tagCompound) : ItemStack.EMPTY);
            }
        }
    }

    public int getMaxStackSize(int slot) {
        return 64;
    }

    @Override
    public void update() {

    }

    private class TileStackHandler extends ItemStackHandler {
        public TileStackHandler(int slots) {
            super(slots);
        }

        @Override
        public int getSlotLimit(int slot) {
            return TileEntityBaubleBag.this.getMaxStackSize(slot);
        }

        @Override
        protected void onContentsChanged(int slot) {
            super.onContentsChanged(slot);
            TileEntityBaubleBag.this.markDirty();
        }
    }
}
