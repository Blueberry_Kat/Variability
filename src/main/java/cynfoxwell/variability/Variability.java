package cynfoxwell.variability;

import cynfoxwell.variability.config.VariabilityConfig;
import cynfoxwell.variability.inventory.GuiHandler;
import cynfoxwell.variability.item.ModItems;
import cynfoxwell.variability.proxy.CommonProxy;
import cynfoxwell.variability.recipe.RecipesFruitPhone;
import cynfoxwell.variability.recipe.RecipesThermionics;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemStack;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@Mod(
        modid = Variability.MODID,
        name = Variability.NAME,
        version = Variability.VERSION,
        useMetadata = true,
        guiFactory = "cynfoxwell.variability.config.ConfigGuiFactory",
        dependencies = "required-after:forge@[14.23.5.2768,);after:baubles@[1.5.2,);after:botania",
        acceptedMinecraftVersions = "[1.12.2]"
)
public class Variability {
    public static final String MODID = "variability";
    public static final String NAME = "Variability";
    public static final String VERSION = "@VERSION@";

    @Mod.Instance(MODID)
    public static Variability INSTANCE;

    @SidedProxy(serverSide = "cynfoxwell.variability.proxy.CommonProxy", clientSide = "cynfoxwell.variability.proxy.ClientProxy")
    public static CommonProxy proxy;

    public static final Logger LOGGER = LogManager.getLogger(NAME);

    public static boolean botaniaLoaded = Loader.isModLoaded("botania");

    @Mod.EventHandler
    public void preInit(FMLPreInitializationEvent e){
        proxy.preInit(e);

        VariabilityConfig.preInit(e);

        if(Loader.isModLoaded("thermionics")) {
            MinecraftForge.EVENT_BUS.register(RecipesThermionics.class);
        }
        if(Loader.isModLoaded("fruitphone")) {
            MinecraftForge.EVENT_BUS.register(RecipesFruitPhone.class);
        }
    }

    @Mod.EventHandler
    public void init(FMLInitializationEvent e){
        proxy.init(e);
        GuiHandler.init();
    }

    @Mod.EventHandler
    public void postInit(FMLPostInitializationEvent e){
        proxy.postInit(e);

        VariabilityConfig.postInit(e);
    }

    public static CreativeTabs tabVariability = new CreativeTabs("tabVariability"){
        @Override
        public ItemStack getTabIconItem(){
            return new ItemStack(ModItems.chromaticDye);
        }
    };

    @SubscribeEvent
    public void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event) {
        if (event.getModID().equals(Variability.MODID)) {
            VariabilityConfig.saveConfig();
        }
    }
}
