package cynfoxwell.variability.network;

import cynfoxwell.variability.Variability;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketHandler {
    public static final SimpleNetworkWrapper INSTANCE = NetworkRegistry.INSTANCE.newSimpleChannel(Variability.MODID);
    private static int packetId = 0;

    public static void registerMessages() {
        registerMessage(PacketPreciseDye.Handler.class, PacketPreciseDye.class, Side.SERVER);
    }

    private static void registerMessage(Class handler, Class packet, Side side) {
        INSTANCE.registerMessage(handler, packet, packetId++, side);
    }
}
