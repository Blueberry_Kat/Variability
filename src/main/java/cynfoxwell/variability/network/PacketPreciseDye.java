package cynfoxwell.variability.network;

import cynfoxwell.variability.item.ItemPreciseDye;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumHand;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketPreciseDye implements IMessage {

    int red,green,blue;

    @Override
    public void fromBytes(ByteBuf buf) {
        red = buf.readInt();
        green = buf.readInt();
        blue = buf.readInt();
    }

    @Override
    public void toBytes(ByteBuf buf) {
        buf.writeInt(red);
        buf.writeInt(green);
        buf.writeInt(blue);
    }

    public PacketPreciseDye() {
    }

    public PacketPreciseDye(int r, int g, int b) {
        red = r;
        green = g;
        blue = b;
    }

    public static class Handler implements IMessageHandler<PacketPreciseDye, IMessage> {
        @Override
        public IMessage onMessage(PacketPreciseDye message, MessageContext ctx) {
            FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> handle(message, ctx));
            return null;
        }

        private void handle(PacketPreciseDye message, MessageContext ctx) {
            EntityPlayerMP playerEntity = ctx.getServerHandler().player;
            ItemStack heldItem = playerEntity.getHeldItem(EnumHand.MAIN_HAND);
            if (!(heldItem.getItem() instanceof ItemPreciseDye)) {
                heldItem = playerEntity.getHeldItemOffhand();
                if (!(heldItem.getItem() instanceof ItemPreciseDye)) {
                    return;
                }
            }

            int col = ((message.red&0x0ff)<<16)|((message.green&0x0ff)<<8)|(message.blue&0x0ff);

            ((ItemPreciseDye)heldItem.getItem()).setColor(heldItem, col);
        }
    }
}
