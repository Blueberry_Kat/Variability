package cynfoxwell.variability.client;

import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import com.elytradev.concrete.common.Rendering;
import cynfoxwell.variability.item.bauble.ItemBaubleBag;
import cynfoxwell.variability.item.bauble.ItemProgrammingSocks;
import cynfoxwell.variability.util.ColorList;
import cynfoxwell.variability.util.ColorUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerArmorBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.ItemStackHandler;

@SideOnly(Side.CLIENT)
public class LayerProgrammingSocks extends LayerArmorBase<ModelBiped> implements LayerRenderer<EntityLivingBase> {
    private ResourceLocation texMain = new ResourceLocation("variability","textures/armor/thighhighs.png");
    private ResourceLocation texOverlay = new ResourceLocation("variability","textures/armor/thighhighs_overlay.png");

    protected final RenderLivingBase<?> player;
    private static ModelBiped model;

    public LayerProgrammingSocks(RenderLivingBase<?> player){
        super(player);
        this.player = player;
        this.initArmor();
    }

    @Override
    public void doRenderLayer(EntityLivingBase entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        ItemStack stack = null;
        ItemStack bag = null;

        IBaublesItemHandler baubles = BaublesApi.getBaublesHandler((EntityPlayer) entity);
        for (int i = 0; i < baubles.getSlots(); i++) {
            ItemStack slot = baubles.getStackInSlot(i);
            if (slot != null && slot != ItemStack.EMPTY) {
                if(slot.getItem() instanceof ItemProgrammingSocks) {
                    stack = slot;
                    break;
                }
                if(slot.getItem() instanceof ItemBaubleBag) {
                    bag = slot;
                    break;
                }
            }
        }

        if (bag != null && stack == null) {
            NBTTagCompound compound = bag.getTagCompound();
            if(compound == null){
                return;
            }

            ItemStackHandler inv = new ItemStackHandler(18);
            ItemBaubleBag.loadSlotsFromNBT(inv,bag);

            for(int i = 0; i < inv.getSlots(); i++) {
                ItemStack slot = inv.getStackInSlot(i);
                if(slot != ItemStack.EMPTY && slot.getItem() instanceof ItemProgrammingSocks){
                    stack = slot;
                    break;
                }
            }
        }

        if(stack != null){
            int color = ((ItemProgrammingSocks)stack.getItem()).getColor(stack);
            int stripeCol = stack.getItem().getDamage(stack) < 16 ? ColorList.values()[stack.getItem().getDamage(stack)].color : (stack.getItem().getDamage(stack) == 16 ? ColorUtil.RainbowEffect(2,1,1) : ColorUtil.RainbowEffect(2,0.5F,1));

            model = getArmorModelHook(entity, stack, EntityEquipmentSlot.LEGS, model);
            model.setModelAttributes(this.player.getMainModel());
            model.setLivingAnimations(entity, limbSwing, limbSwingAmount, partialTicks);

            Rendering.color3(color);
            Minecraft.getMinecraft().renderEngine.bindTexture(texMain);
            model.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);

            Rendering.color3(stripeCol);
            Minecraft.getMinecraft().renderEngine.bindTexture(texOverlay);
            model.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
            GlStateManager.color(1F,1F,1F);
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }

    @Override
    protected void initArmor() {
        this.model = new ModelBiped(0.25F);
    }

    @Override
    protected void setModelSlotVisible(ModelBiped model, EntityEquipmentSlot slotIn) {
        this.setModelVisible(model);

        model.bipedRightLeg.showModel = true;
        model.bipedLeftLeg.showModel = true;
    }

    protected void setModelVisible(ModelBiped model)
    {
        model.setVisible(false);
    }
}
