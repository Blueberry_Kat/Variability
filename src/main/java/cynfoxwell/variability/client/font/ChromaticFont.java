package cynfoxwell.variability.client.font;

import com.mojang.realmsclient.gui.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ChromaticFont extends FontRenderer {
    private static ChromaticFont INSTANCE;

    public static ChromaticFont get() {
        return INSTANCE;
    }

    static {
        Minecraft mc = Minecraft.getMinecraft();
        ResourceLocation ascii = new ResourceLocation("minecraft:textures/font/ascii.png");
        INSTANCE = new ChromaticFont(mc.gameSettings, ascii, mc.renderEngine, false);

        ((IReloadableResourceManager) mc.getResourceManager()).registerReloadListener(INSTANCE);
    }

    private ChromaticFont(GameSettings gameSettingsIn, ResourceLocation location, TextureManager textureManagerIn, boolean unicode) {
        super(gameSettingsIn, location, textureManagerIn, unicode);
    }

    @Override
    public int drawString(String text, float x, float y, int color, boolean dropShadow) {
        String text2 = ChatFormatting.stripFormatting(text);

        if(text2.startsWith("\uD83C\uDF08") && text2.endsWith("\uD83C\uDF08")) {
            text2 = text2.replaceAll("\uD83C\uDF08", "");
            float posX = x;
            float huehuehue = (System.nanoTime() * 2) / 9E9F % 1F;

            for (int i = 0; i < text2.length(); i++) {
                int c = (color & 0xFF000000) | MathHelper.hsvToRGB(huehuehue, 1, 1);


                posX = super.drawString(String.valueOf(text2.charAt(i)), posX, y, c, dropShadow) - 1;

                huehuehue += 0.05;
                huehuehue %= 1;
            }

            return (int) posX;
        }
        else if(text2.startsWith("\u2b50") && text2.endsWith("\u2b50")) {
            text2 = text2.replaceAll("\u2b50", "");
            float posX = x;
            float huehuehue = (System.nanoTime()*2)/9E9F % 1F;

            for(int i = 0; i < text2.length(); i++) {
                int c = (color & 0xFF000000) | MathHelper.hsvToRGB(huehuehue, 0.5F, 1);


                posX = super.drawString(String.valueOf(text2.charAt(i)), posX, y, c, dropShadow) - 1;

                huehuehue += 0.05;
                huehuehue %= 1;
            }

            return (int) posX;
        }else{
            return super.drawString(text, x, y, color, dropShadow);
        }
    }
}