package cynfoxwell.variability.client.font;

import com.mojang.realmsclient.gui.ChatFormatting;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.client.renderer.texture.TextureManager;
import net.minecraft.client.resources.IReloadableResourceManager;
import net.minecraft.client.settings.GameSettings;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.math.MathHelper;

public class ChromaticNameFont extends FontRenderer {
    private static ChromaticNameFont INSTANCE;
    boolean first = false;

    public static ChromaticNameFont get() {
        INSTANCE.first = true;
        return INSTANCE;
    }

    static {
        Minecraft mc = Minecraft.getMinecraft();
        ResourceLocation ascii = new ResourceLocation("minecraft:textures/font/ascii.png");
        INSTANCE = new ChromaticNameFont(mc.gameSettings, ascii, mc.renderEngine, false);

        ((IReloadableResourceManager) mc.getResourceManager()).registerReloadListener(INSTANCE);
    }

    private ChromaticNameFont(GameSettings gameSettingsIn, ResourceLocation location, TextureManager textureManagerIn, boolean unicode) {
        super(gameSettingsIn, location, textureManagerIn, unicode);
    }

    @Override
    public int drawString(String text, float x, float y, int color, boolean dropShadow) {
        if(!first) return super.drawString(text, x, y, color, dropShadow);
        first = false;

        float posX = x;
        float huehuehue = (System.nanoTime()*2)/9E9F % 1F;

        String textRender = ChatFormatting.stripFormatting(text);

        for(int i = 0; i < textRender.length(); i++) {
            int c = (color & 0xFF000000) | MathHelper.hsvToRGB(huehuehue, 1, 1);


            posX = super.drawString(String.valueOf(textRender.charAt(i)), posX, y, c, dropShadow) - 1;

            huehuehue += 0.05;
            huehuehue %= 1;
        }

        return (int) posX;
    }
}
