package cynfoxwell.variability.client;

import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import com.elytradev.concrete.common.Rendering;
import cynfoxwell.variability.item.bauble.ItemBaubleBag;
import cynfoxwell.variability.item.bauble.ItemScarf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ModelBiped;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.RenderLivingBase;
import net.minecraft.client.renderer.entity.layers.LayerArmorBase;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.inventory.EntityEquipmentSlot;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.items.ItemStackHandler;

public class LayerScarf extends LayerArmorBase<ModelBiped> implements LayerRenderer<EntityLivingBase> {
    private ResourceLocation texture = new ResourceLocation("variability", "textures/armor/scarf.png");

    protected final RenderLivingBase<?> player;
    private static ModelBiped model;

    public LayerScarf(RenderLivingBase<?> player) {
        super(player);
        this.player = player;
        this.initArmor();
    }

    @Override
    public void doRenderLayer(EntityLivingBase entity, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {
        ItemStack stack = null;
        ItemStack bag = null;

        IBaublesItemHandler baubles = BaublesApi.getBaublesHandler((EntityPlayer) entity);
        for (int i = 0; i < baubles.getSlots(); i++) {
            ItemStack slot = baubles.getStackInSlot(i);
            if (slot != null && slot != ItemStack.EMPTY) {
                if(slot.getItem() instanceof ItemScarf) {
                    stack = slot;
                    break;
                }
                if(slot.getItem() instanceof ItemBaubleBag) {
                    bag = slot;
                    break;
                }
            }
        }

        if (bag != null && stack == null) {
            NBTTagCompound compound = bag.getTagCompound();
            if(compound == null){
                return;
            }

            ItemStackHandler inv = new ItemStackHandler(18);
            ItemBaubleBag.loadSlotsFromNBT(inv,bag);

            for(int i = 0; i < inv.getSlots(); i++) {
                ItemStack slot = inv.getStackInSlot(i);
                if(slot != ItemStack.EMPTY && slot.getItem() instanceof ItemScarf){
                    stack = slot;
                    break;
                }
            }
        }

        if (stack != null) {
            int color = ((ItemScarf)stack.getItem()).getColor(stack);

            model = getArmorModelHook(entity, stack, EntityEquipmentSlot.CHEST, model);
            model.setModelAttributes(this.player.getMainModel());
            model.setLivingAnimations(entity, limbSwing, limbSwingAmount, partialTicks);

            Rendering.color3(color);
            Minecraft.getMinecraft().renderEngine.bindTexture(texture);
            model.render(entity, limbSwing, limbSwingAmount, ageInTicks, netHeadYaw, headPitch, scale);
            if (stack.hasEffect())
            {
                renderEnchantedGlint(player, entity, model, limbSwing, limbSwingAmount, partialTicks, ageInTicks, netHeadYaw, headPitch, scale);
            }
            GlStateManager.color(1F,1F,1F);
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }

    @Override
    protected void initArmor() {
        this.model = new ModelBiped(0.25F);
    }

    @Override
    protected void setModelSlotVisible(ModelBiped model, EntityEquipmentSlot slotIn) {
        this.setModelVisible(model);

        model.bipedRightArm.showModel = true;
        model.bipedLeftArm.showModel = true;
    }

    protected void setModelVisible(ModelBiped model) {
        model.setVisible(false);
    }
}
