package cynfoxwell.variability.client;

import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import baubles.api.render.IRenderBauble;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.item.bauble.ItemBaubleBag;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.entity.layers.LayerRenderer;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.MobEffects;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import org.lwjgl.opengl.GL11;
import vazkii.botania.api.item.IBaubleRender;

import javax.annotation.Nonnull;

public class LayerBaubleBag implements LayerRenderer<EntityPlayer> {

    @Override
    public void doRenderLayer(@Nonnull EntityPlayer player, float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw, float headPitch, float scale) {

        if(player.getActivePotionEffect(MobEffects.INVISIBILITY) != null)
            return;

        IBaublesItemHandler binv = BaublesApi.getBaublesHandler(player);

        for(int i = 0; i < binv.getSlots(); i++) {
            ItemStack stack = binv.getStackInSlot(i);
            if (stack != null && !stack.isEmpty()) {
                Item item = stack.getItem();
                if (item instanceof ItemBaubleBag) {
                    ItemStackHandler inv = new ItemStackHandler(18);
                    ItemBaubleBag.loadSlotsFromNBT(inv, stack);

                    dispatchRenders(inv, player, IRenderBauble.RenderType.BODY, partialTicks);

                    float yaw = player.prevRotationYawHead + (player.rotationYawHead - player.prevRotationYawHead) * partialTicks;
                    float yawOffset = player.prevRenderYawOffset + (player.renderYawOffset - player.prevRenderYawOffset) * partialTicks;
                    float pitch = player.prevRotationPitch + (player.rotationPitch - player.prevRotationPitch) * partialTicks;

                    GlStateManager.pushMatrix();
                    GlStateManager.rotate(yawOffset, 0, -1, 0);
                    GlStateManager.rotate(yaw - 270, 0, 1, 0);
                    GlStateManager.rotate(pitch, 0, 0, 1);
                    dispatchRenders(inv, player, IRenderBauble.RenderType.HEAD, partialTicks);
                    GlStateManager.popMatrix();
                }
            }
        }
    }

    private void dispatchRenders(ItemStackHandler inv, EntityPlayer player, IRenderBauble.RenderType type, float partialTicks) {
        for(int i = 0; i < inv.getSlots(); i++) {
            ItemStack stack = inv.getStackInSlot(i);
            if(stack != null && !stack.isEmpty()) {
                Item item = stack.getItem();
                if(item instanceof IRenderBauble) {
                    GlStateManager.pushMatrix();
                    GL11.glColor3ub((byte) 255, (byte) 255, (byte) 255);
                    GlStateManager.color(1F, 1F, 1F, 1F);
                    ((IRenderBauble) stack.getItem()).onPlayerBaubleRender(stack, player, type, partialTicks);
                    GlStateManager.popMatrix();
                }
                if(Variability.botaniaLoaded && item instanceof IBaubleRender) {
                    GlStateManager.pushMatrix();
                    GL11.glColor3ub((byte) 255, (byte) 255, (byte) 255);
                    GlStateManager.color(1F, 1F, 1F, 1F);
                    ((IBaubleRender) stack.getItem()).onPlayerBaubleRender(stack, player, type == IRenderBauble.RenderType.BODY ? IBaubleRender.RenderType.BODY : IBaubleRender.RenderType.HEAD, partialTicks);
                    GlStateManager.popMatrix();
                }
            }
        }
    }

    @Override
    public boolean shouldCombineTextures() {
        return false;
    }
}
