package cynfoxwell.variability.inventory;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.inventory.gui.GuiBaubleBag;
import cynfoxwell.variability.inventory.gui.GuiPreciseDye;
import cynfoxwell.variability.item.ItemPreciseDye;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.network.NetworkRegistry;

import javax.annotation.Nullable;

public class GuiHandler implements IGuiHandler {
    public static void init(){
        NetworkRegistry.INSTANCE.registerGuiHandler(Variability.INSTANCE, new GuiHandler());
    }

    @Nullable
    @Override
    public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tile = null;
        if(GuiTypes.values()[id].checkTileEntity){
            tile = world.getTileEntity(new BlockPos(x, y, z));
        }

        switch(GuiTypes.values()[id]){
            case BAUBLE_BAG:
                return new ContainerBaubleBag(player.inventory);
            default:
                return null;
        }
    }

    @Nullable
    @Override
    public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
        TileEntity tile = null;
        if(GuiTypes.values()[id].checkTileEntity){
            tile = world.getTileEntity(new BlockPos(x, y, z));
        }

        switch(GuiTypes.values()[id]){
            case BAUBLE_BAG:
                return new GuiBaubleBag(player.inventory);
            case PRECISE_DYE:
                if (player.getHeldItemMainhand().getItem() instanceof ItemPreciseDye)
                    return new GuiPreciseDye(player.getHeldItemMainhand());
                else if (player.getHeldItemOffhand().getItem() instanceof ItemPreciseDye)
                    return new GuiPreciseDye(player.getHeldItemOffhand());
                else
                    return null;
            default:
                return null;
        }
    }

    public enum GuiTypes {
        BAUBLE_BAG(false),
        PRECISE_DYE(false);

        public final boolean checkTileEntity;

        GuiTypes(){
            this(true);
        }

        GuiTypes(boolean checkTileEntity){
            this.checkTileEntity = checkTileEntity;
        }
    }
}
