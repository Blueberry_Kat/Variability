package cynfoxwell.variability.inventory.gui;

import cynfoxwell.variability.inventory.gui.elements.GuiNumberField;
import cynfoxwell.variability.item.ItemPreciseDye;
import cynfoxwell.variability.network.PacketHandler;
import cynfoxwell.variability.network.PacketPreciseDye;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.resources.I18n;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import java.awt.*;
import java.io.IOException;

@SideOnly(Side.CLIENT)
public class GuiPreciseDye extends GuiScreen {
    private GuiTextField inpRed;
    private GuiTextField inpGreen;
    private GuiTextField inpBlue;

    private ItemStack item;

    public GuiPreciseDye(ItemStack item) {
        super();

        this.item = item;
    }

    @Override
    public void initGui() {
        super.initGui();

        int guiLeft = this.width / 2;
        int guiTop = this.height / 2 - 50;

        int initColInt = ((ItemPreciseDye)item.getItem()).getColor(item);
        Color initColor = new Color(initColInt);

        this.allowUserInput = true;

        this.inpRed = new GuiNumberField(0, this.fontRenderer, guiLeft-24, guiTop + 20, 48, 10);
        this.inpRed.setTextColor(-1);
        this.inpRed.setDisabledTextColour(-1);
        this.inpRed.setMaxStringLength(3);
        this.inpRed.setText(String.valueOf(initColor.getRed()));

        this.inpGreen = new GuiNumberField(1, this.fontRenderer, guiLeft-24, guiTop + 34, 48, 10);
        this.inpGreen.setTextColor(-1);
        this.inpGreen.setDisabledTextColour(-1);
        this.inpGreen.setMaxStringLength(3);
        this.inpGreen.setText(String.valueOf(initColor.getGreen()));

        this.inpBlue = new GuiNumberField(2, this.fontRenderer, guiLeft-24, guiTop + 48, 48, 10);
        this.inpBlue.setTextColor(-1);
        this.inpBlue.setDisabledTextColour(-1);
        this.inpBlue.setMaxStringLength(3);
        this.inpBlue.setText(String.valueOf(initColor.getBlue()));

        this.buttonList.add(new GuiButton(1, guiLeft-44, guiTop + 64, 40, 20, I18n.format("gui.done")));
        this.buttonList.add(new GuiButton(2, guiLeft+4, guiTop + 64, 40, 20, I18n.format("gui.cancel")));

        this.buttonList.add(new GuiButton(3, guiLeft-36, guiTop + 20, 10, 10, "-"));
        this.buttonList.add(new GuiButton(4, guiLeft+26, guiTop + 20, 10, 10, "+"));
        this.buttonList.add(new GuiButton(5, guiLeft-36, guiTop + 34, 10, 10, "-"));
        this.buttonList.add(new GuiButton(6, guiLeft+26, guiTop + 34, 10, 10, "+"));
        this.buttonList.add(new GuiButton(7, guiLeft-36, guiTop + 48, 10, 10, "-"));
        this.buttonList.add(new GuiButton(8, guiLeft+26, guiTop + 48, 10, 10, "+"));
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.inpRed.drawTextBox();
        this.inpGreen.drawTextBox();
        this.inpBlue.drawTextBox();

        super.drawScreen(mouseX, mouseY, partialTicks);

        int col = new Color(
                Integer.parseInt(inpRed.getText().equals("") ? "0" : inpRed.getText()),
                Integer.parseInt(inpGreen.getText().equals("") ? "0" : inpGreen.getText()),
                Integer.parseInt(inpBlue.getText().equals("") ? "0" : inpBlue.getText())
        ).getRGB();

        drawRect(this.width / 2 - 16, this.height / 2 - 50 - 16, this.width / 2 + 16, this.height / 2 - 50 + 16, col);
    }

    @Override
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        if (
                this.inpRed.textboxKeyTyped(typedChar, keyCode)
                || this.inpGreen.textboxKeyTyped(typedChar, keyCode)
                || this.inpBlue.textboxKeyTyped(typedChar, keyCode)
        ) {

        } else {
            super.keyTyped(typedChar, keyCode);
        }
    }

    @Override
    public void updateScreen() {
        super.updateScreen();
    }

    @Override
    public boolean doesGuiPauseGame() {
        return false;
    }

    @Override
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        if (this.inpRed.mouseClicked(mouseX, mouseY, mouseButton)) {
            inpRed.setFocused(true);
        } else if (this.inpGreen.mouseClicked(mouseX, mouseY, mouseButton)) {
            inpGreen.setFocused(true);
        } else if (this.inpBlue.mouseClicked(mouseX, mouseY, mouseButton)) {
            inpBlue.setFocused(true);
        } else {
            super.mouseClicked(mouseX, mouseY, mouseButton);
        }
    }

    @Override
    protected void actionPerformed(GuiButton b) {
        int value = 0;

        switch(b.id){
            case 1:
                PacketHandler.INSTANCE.sendToServer(
                        new PacketPreciseDye(
                                Integer.parseInt(inpRed.getText().equals("") ? "0" : inpRed.getText()),
                                Integer.parseInt(inpGreen.getText().equals("") ? "0" : inpGreen.getText()),
                                Integer.parseInt(inpBlue.getText().equals("") ? "0" : inpBlue.getText())
                        ));

                this.mc.player.closeScreen();
                break;
            case 2:
                this.mc.player.closeScreen();
                break;
            case 3:
                value = Integer.parseInt(inpRed.getText());
                value--;
                if(value < 0) value = 0;

                inpRed.setText(String.valueOf(value));
                break;
            case 4:
                value = Integer.parseInt(inpRed.getText());
                value++;
                if(value > 255) value = 255;

                inpRed.setText(String.valueOf(value));
                break;
            case 5:
                value = Integer.parseInt(inpGreen.getText());
                value--;
                if(value < 0) value = 0;

                inpGreen.setText(String.valueOf(value));
                break;
            case 6:
                value = Integer.parseInt(inpGreen.getText());
                value++;
                if(value > 255) value = 255;

                inpGreen.setText(String.valueOf(value));
                break;
            case 7:
                value = Integer.parseInt(inpBlue.getText());
                value--;
                if(value < 0) value = 0;

                inpBlue.setText(String.valueOf(value));
                break;
            case 8:
                value = Integer.parseInt(inpBlue.getText());
                value++;
                if(value > 255) value = 255;

                inpBlue.setText(String.valueOf(value));
                break;
            default:
                break;
        }
    }
}
