package cynfoxwell.variability.inventory.gui;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.inventory.ContainerBaubleBag;
import net.minecraft.client.gui.inventory.GuiContainer;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.resources.I18n;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.util.ResourceLocation;

public class GuiBaubleBag extends GuiContainer {
    private static final ResourceLocation GUI_TEXTURE = new ResourceLocation(Variability.MODID,"textures/gui/bauble_bag.png");

    public GuiBaubleBag(InventoryPlayer inventory) {
        super(new ContainerBaubleBag(inventory));

        this.xSize = 176;
        this.ySize = 150;
    }

    @Override
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        this.drawDefaultBackground();
        super.drawScreen(mouseX, mouseY, partialTicks);
        this.renderHoveredToolTip(mouseX, mouseY);
    }

    @Override
    protected void drawGuiContainerForegroundLayer(int mouseX, int mouseY) {
        this.fontRenderer.drawString(I18n.format("container.variability.bauble_bag.name"), 8, 6, 12632256);
        this.fontRenderer.drawString(I18n.format("container.inventory"), 8, this.ySize - 96 + 2, 12632256);
    }

    @Override
    protected void drawGuiContainerBackgroundLayer(float partialTicks, int mouseX, int mouseY) {
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        this.mc.getTextureManager().bindTexture(GUI_TEXTURE);
        this.drawTexturedModalRect(this.guiLeft, this.guiTop, 0, 0, this.xSize, this.ySize);
    }
}
