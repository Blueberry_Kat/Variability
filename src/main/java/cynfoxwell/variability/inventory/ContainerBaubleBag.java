package cynfoxwell.variability.inventory;

import baubles.api.IBauble;
import cynfoxwell.variability.inventory.slot.SlotImmovable;
import cynfoxwell.variability.item.bauble.ItemBaubleBag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.ClickType;
import net.minecraft.inventory.Container;
import net.minecraft.inventory.Slot;
import net.minecraft.item.ItemStack;
import net.minecraftforge.items.ItemStackHandler;
import net.minecraftforge.items.SlotItemHandler;

public class ContainerBaubleBag extends Container {
    private ItemStackHandler bagInv = new ItemStackHandler(18);
    private InventoryPlayer inventory;

    public ContainerBaubleBag(InventoryPlayer inventory) {
        this.inventory = inventory;

        for(int y = 0; y < 2; y++){
            for(int x = 0; x < 9; x++){
                this.addSlotToContainer(new SlotItemHandler(bagInv, x + y * 9, 8 + x * 18, 18 + y * 18){
                    @Override
                    public boolean isItemValid(ItemStack stack){
                        return stack != null && stack != ItemStack.EMPTY && stack.getItem() instanceof IBauble && !(stack.getItem() instanceof ItemBaubleBag);
                    }
                });
            }
        }

        for(int y = 0; y < 3; y++){
            for(int x = 0; x < 9; x++){
                this.addSlotToContainer(new Slot(inventory, x + y * 9 + 9, 8 + x * 18, 68 + y * 18));
            }
        }
        for(int i = 0; i < 9; i++){
            if(i == inventory.currentItem){
                this.addSlotToContainer(new SlotImmovable(inventory, i, 8 + i * 18, 126));
            }
            else{
                this.addSlotToContainer(new Slot(inventory, i, 8 + i * 18, 126));
            }
        }

        ItemStack stack = inventory.getCurrentItem();
        if(stack != null && stack != ItemStack.EMPTY && stack.getItem() instanceof ItemBaubleBag){
            ItemBaubleBag.loadSlotsFromNBT(this.bagInv, inventory.getCurrentItem());
        }
    }

    @Override
    public ItemStack transferStackInSlot(EntityPlayer player, int slot){
        int inventoryStart = 18;
        int inventoryEnd = inventoryStart+26;
        int hotbarStart = inventoryEnd+1;
        int hotbarEnd = hotbarStart+8;

        Slot theSlot = this.inventorySlots.get(slot);

        if(theSlot != null && theSlot.getHasStack()){
            ItemStack newStack = theSlot.getStack();
            ItemStack currentStack = newStack.copy();

            if(slot >= inventoryStart){
                if(newStack != null && newStack != ItemStack.EMPTY && newStack.getItem() instanceof IBauble && !(newStack.getItem() instanceof ItemBaubleBag)){
                    if(!this.mergeItemStack(newStack, 0, 18, false)){
                        return ItemStack.EMPTY;
                    }
                }

                else if(slot >= inventoryStart && slot <= inventoryEnd){
                    if(!this.mergeItemStack(newStack, hotbarStart, hotbarEnd+1, false)){
                        return ItemStack.EMPTY;
                    }
                }
                else if(slot >= inventoryEnd+1 && slot < hotbarEnd+1 && !this.mergeItemStack(newStack, inventoryStart, inventoryEnd+1, false)){
                    return ItemStack.EMPTY;
                }
            }
            else if(!this.mergeItemStack(newStack, inventoryStart, hotbarEnd+1, false)){
                return ItemStack.EMPTY;
            }

            if(newStack != null && newStack != ItemStack.EMPTY){
                theSlot.putStack(ItemStack.EMPTY);
            }
            else{
                theSlot.onSlotChanged();
            }

            if(newStack.getCount() == currentStack.getCount()){
                return ItemStack.EMPTY;
            }
            theSlot.onTake(player, newStack);

            return currentStack;
        }
        return ItemStack.EMPTY;
    }

    @Override
    public ItemStack slotClick(int slotId, int dragType, ClickType clickTypeIn, EntityPlayer player){
        if(clickTypeIn == ClickType.SWAP && dragType == this.inventory.currentItem){
            return ItemStack.EMPTY;
        }
        else{
            return super.slotClick(slotId, dragType, clickTypeIn, player);
        }
    }

    @Override
    public void onContainerClosed(EntityPlayer player){
        ItemStack stack = this.inventory.getCurrentItem();
        if(stack != null && stack != ItemStack.EMPTY && stack.getItem() instanceof ItemBaubleBag){
            ItemBaubleBag.writeSlotsToNBT(this.bagInv, this.inventory.getCurrentItem());
        }
        super.onContainerClosed(player);
    }

    @Override
    public boolean canInteractWith(EntityPlayer playerIn) {
        return true;
    }
}
