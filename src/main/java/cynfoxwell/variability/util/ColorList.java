package cynfoxwell.variability.util;

import net.minecraft.util.IStringSerializable;

public enum ColorList implements IStringSerializable {

    WHITE("White", "white", 15330540),
    ORANGE("Orange", "orange", 15758867),
    MAGENTA("Magenta", "magenta", 12403891),
    LIGHT_BLUE("LightBlue", "light_blue", 3846105),
    YELLOW("Yellow", "yellow", 16303655),
    LIME("Lime", "lime", 7387417),
    PINK("Pink", "pink", 15568300),
    GRAY("Gray", "gray", 4080711),
    LIGHT_GRAY("LightGray", "light_gray", 9342598),
    CYAN("Cyan", "cyan", 1411473),
    PURPLE("Purple", "purple", 7940780),
    BLUE("Blue", "blue", 3488157),
    BROWN("Brown", "brown", 7489320),
    GREEN("Green", "green", 5532955),
    RED("Red", "red", 10561314),
    BLACK("Black", "black", 1316121);

    public String regName;
    public String oreName;
    public int color;

    ColorList(String oreName, String regName, int color) {
        this.oreName = oreName;
        this.regName = regName;
        this.color = color;
    }

    public static ColorList getColorFromDyeName(String color) {
        if(color.substring(0, 3).equals("dye")){
            String actualName = color.substring(3);
            for(int i = 0; i < values().length; i++){
                String aName = values()[i].oreName;
                if(aName != null){
                    if(aName.equalsIgnoreCase(actualName)){
                        return values()[i];
                    }
                }
            }
        }
        return null;
    }

    @Override
    public String getName() {
        return this.regName;
    }

    public int getColor() {
        return this.color;
    }
}
