package cynfoxwell.variability.util;

import net.minecraft.client.Minecraft;
import net.minecraft.util.text.TextFormatting;

import static net.minecraft.util.text.TextFormatting.*;

public class TextUtil {
    private static final TextFormatting[] rainbow = new TextFormatting[] { RED, DARK_RED, GOLD, YELLOW, GREEN, DARK_GREEN, DARK_AQUA, AQUA, BLUE, DARK_BLUE, DARK_PURPLE, LIGHT_PURPLE };

    public static String RainbowFormat(String input) {
        return ludicrousFormatting(input, rainbow, 80.0, 1, 1);
    }

    //Stolen from Avaritia cause I can't do any of this :upside_down:
    public static String ludicrousFormatting(String input, TextFormatting[] colours, double delay, int step, int posstep) {
        StringBuilder sb = new StringBuilder(input.length() * 3);
        if (delay <= 0) {
            delay = 0.001;
        }

        int offset = (int) Math.floor(Minecraft.getSystemTime() / delay) % colours.length;

        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);

            int col = ((i * posstep) + colours.length - offset) % colours.length;

            sb.append(colours[col].toString());
            sb.append(c);
        }

        return sb.toString();
    }

    public static String formatColors(String input){
        return input.replaceAll("&","\u00a7");
    }
}

