package cynfoxwell.variability.util;

import java.awt.*;

public class ColorUtil {
    public static int HextoRGB(String str) {
        return new Color(
                Integer.valueOf( str.substring( 0, 2 ), 16 ),
                Integer.valueOf( str.substring( 2, 4 ), 16 ),
                Integer.valueOf( str.substring( 4, 6 ), 16 ) ).getRGB();
    }

    public static int RainbowEffect(int speed, float sat, float val){
        return Color.HSBtoRGB((System.nanoTime()*speed)/9E9F % 1F,sat, val);
    }

    public static int RainbowEffect(float speed, float sat, float val){
        return Color.HSBtoRGB((System.nanoTime()*speed)/9E9F % 1F,sat, val);
    }
}
