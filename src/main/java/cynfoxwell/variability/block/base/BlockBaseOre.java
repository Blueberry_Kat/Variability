package cynfoxwell.variability.block.base;


import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class BlockBaseOre extends BlockBase {
    public BlockBaseOre(String name) {
        super(name, Material.ROCK);
        setHardness(3);
        setResistance(5);
        setSoundType(SoundType.STONE);
        setHarvestLevel("pickaxe", 2);
    }
}
