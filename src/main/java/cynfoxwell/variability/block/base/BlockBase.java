package cynfoxwell.variability.block.base;

import cynfoxwell.variability.Variability;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockBase extends Block {
    public BlockBase(String name, Material blockMaterialIn, MapColor blockMapColorIn) {
        super(blockMaterialIn, blockMapColorIn);
        setCreativeTab(Variability.tabVariability);
        setRegistryName(new ResourceLocation(Variability.MODID, name));
        setUnlocalizedName(Variability.MODID+"."+name);
    }

    public BlockBase(String name, Material blockMaterialIn) {
        super(blockMaterialIn);
        setCreativeTab(Variability.tabVariability);
        setRegistryName(new ResourceLocation(Variability.MODID, name));
        setUnlocalizedName(Variability.MODID+"."+name);
    }

    @SideOnly(Side.CLIENT)
    public void initModel() {
        ModelLoader.setCustomModelResourceLocation(ItemBlockBase.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }
}
