package cynfoxwell.variability.block;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.block.base.BlockBase;
import cynfoxwell.variability.block.base.ItemBlockBase;
import cynfoxwell.variability.config.ConfigHandler;
import net.minecraft.block.Block;
import net.minecraft.block.material.MapColor;
import net.minecraft.block.material.Material;
import net.minecraft.item.Item;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.registries.IForgeRegistry;

@GameRegistry.ObjectHolder(Variability.MODID)
public class ModBlocks {
    @GameRegistry.ObjectHolder("dyable_wool")
    public static BlockDyable dyableWool = (BlockDyable)new BlockDyable("dyable_wool", Material.CLOTH, MapColor.CLOTH).setHardness(0.8F);
    @GameRegistry.ObjectHolder("mimic_wool")
    public static BlockBase mimicWool = (BlockBase)new BlockBase("mimic_wool", Material.CLOTH, MapColor.CLOTH).setHardness(0.8F);

    @GameRegistry.ObjectHolder("crop_flax")
    public static BlockFlaxCrop cropFlax = new BlockFlaxCrop();

    @Mod.EventBusSubscriber(modid = Variability.MODID)
    public static class Registration {
        @SubscribeEvent
        public static void registerBlocks(RegistryEvent.Register<Block> event) {
            IForgeRegistry<Block> registry = event.getRegistry();

            if(ConfigHandler.enableCosmetics){
                //registry.register(dyableWool);
                //registry.register(mimicWool);
            }

            if(ConfigHandler.enableAgriculture) {
                registry.register(cropFlax);
            }

            //GameRegistry.registerTileEntity(TileEntityCompressedChest.class,new ResourceLocation(Variability.MODID,"compressed_chest"));
        }

        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            IForgeRegistry<Item> itemRegistry = event.getRegistry();
            if(ConfigHandler.enableCosmetics) {
                //itemRegistry.register(new ItemBlockBase(dyableWool));
                //itemRegistry.register(new ItemBlockBase(mimicWool));
            }

            if(ConfigHandler.enableAgriculture) {
                itemRegistry.register(new ItemBlockBase(cropFlax));
            }
        }

        @SubscribeEvent
        public static void registerModels (ModelRegistryEvent event) {
            if(ConfigHandler.enableCosmetics) {
                //dyableWool.initModel();
                //mimicWool.initModel();
            }

            if(ConfigHandler.enableAgriculture) {
                cropFlax.initModel();
            }
        }
    }
}
