package cynfoxwell.variability.block;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.block.base.ItemBlockBase;
import cynfoxwell.variability.item.ModItems;
import net.minecraft.block.BlockCrops;
import net.minecraft.block.SoundType;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.init.Items;
import net.minecraft.item.Item;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class BlockFlaxCrop extends BlockCrops {
    public BlockFlaxCrop()
    {
        this.setDefaultState(this.blockState.getBaseState().withProperty(this.getAgeProperty(), Integer.valueOf(0)));
        this.setTickRandomly(true);
        this.setHardness(0.0F);
        this.setSoundType(SoundType.PLANT);
        this.disableStats();

        this.setRegistryName(new ResourceLocation(Variability.MODID, "crop_flax"));
        this.setUnlocalizedName(Variability.MODID+".crop_flax");
    }

    public int getMaxAge()
    {
        return 4;
    }

    protected Item getSeed()
    {
        return ModItems.flaxSeeds;
    }

    protected Item getCrop()
    {
        return Items.STRING;
    }

    @SideOnly(Side.CLIENT)
    public void initModel() {
        ModelLoader.setCustomModelResourceLocation(ItemBlockBase.getItemFromBlock(this), 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }
}
