package cynfoxwell.variability.config;

import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

public class VariabilityConfig {
    public static Configuration config;

    public static void preInit(FMLPreInitializationEvent e){
        config = new Configuration(e.getSuggestedConfigurationFile());
        saveConfig();
    }

    public static void saveConfig(){
        config.load();
        ConfigHandler.enableBBag = config.getBoolean("Enable Bauble Bag", "modules", true, "Enable or disable Bauble Bag.");
        ConfigHandler.enableCosmetics = config.getBoolean("Enable Cosmetic Items", "modules", true, "Enable or disable cosmetic items.");
        ConfigHandler.enablePaxels = config.getBoolean("Enable Paxels", "modules", true, "Enable or disable paxels.");
        ConfigHandler.enableAgriculture = config.getBoolean("Enable Agriculture", "modules", true, "Enable or disable agricultural related features.");

        ConfigHandler.paxelBCT = config.getBoolean("Force Enable Balanced Clay Tools Paxel", "paxels", false, "Forces mod support when mod isn't installed.");
        ConfigHandler.paxelIridescent = config.getBoolean("Force Enable Iridescent Paxels", "paxels", false, "Forces mod support when mod isn't installed.");

        ConfigHandler.agrFlaxSeedWeight = config.getInt("Flax Seed Drop Weight", "agriculture", 5, 0, 1024,"Drop weight of Flax Seeds. Set to 0 to disable drops.");

        ConfigHandler.miscLG2Exp = config.getBoolean("XP Orb Replacement", "misc", true, "Replaces XP orbs with ones from a hidden gem of a 1.7.10 mod.");
        ConfigHandler.miscEmeraldPieces = config.getBoolean("Emerald Pieces", "misc", true, "Adds in emerald pieces that drop from mobs with a 1/64 chance.");

        if (config.hasChanged())
            config.save();
    }

    public static void postInit(FMLPostInitializationEvent e) {
        if (config.hasChanged())
            config.save();
    }
}
