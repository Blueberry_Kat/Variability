package cynfoxwell.variability.config;

import cynfoxwell.variability.Variability;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.common.config.ConfigElement;
import net.minecraftforge.fml.client.IModGuiFactory;
import net.minecraftforge.fml.client.config.GuiConfig;
import net.minecraftforge.fml.client.config.IConfigElement;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class ConfigGuiFactory implements IModGuiFactory {
    public static class ConfigGui extends GuiConfig {

        public ConfigGui(GuiScreen parentScreen) {
            super(parentScreen, getElements(), Variability.MODID, "Variability", false, true, TextFormatting.DARK_PURPLE+"Variability Config");
        }

        private static List<IConfigElement> getElements() {
            List<IConfigElement> elements = new ArrayList<>();

            elements.add(new ConfigElement(VariabilityConfig.config.getCategory("modules")));
            elements.add(new ConfigElement(VariabilityConfig.config.getCategory("paxels")));
            elements.add(new ConfigElement(VariabilityConfig.config.getCategory("agriculture")));
            elements.add(new ConfigElement(VariabilityConfig.config.getCategory("tools")));


            return elements;
        }

    }

    @Override
    public void initialize(Minecraft mc) {

    }

    @Override
    public boolean hasConfigGui() {
        return true;
    }

    @Override
    public GuiScreen createConfigGui(GuiScreen parentScreen) {
        return new ConfigGui(parentScreen);
    }

    @Override
    public Set<RuntimeOptionCategoryElement> runtimeGuiCategories() {
        return null;
    }
}
