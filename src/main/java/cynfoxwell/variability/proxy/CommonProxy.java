package cynfoxwell.variability.proxy;

import baubles.api.BaublesApi;
import baubles.api.cap.IBaublesItemHandler;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.item.bauble.ItemGloves;
import cynfoxwell.variability.network.PacketHandler;
import cynfoxwell.variability.tile.TileEntityBaubleBag;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;

@Mod.EventBusSubscriber
public class CommonProxy {
    public void preInit(FMLPreInitializationEvent e) {
        PacketHandler.registerMessages();
    }

    public void init(FMLInitializationEvent e) {
        ResourceLocation name = new ResourceLocation(Variability.MODID, "TileEntityBaubleBag");
        GameRegistry.registerTileEntity(TileEntityBaubleBag.class, name);
    }

    public void postInit(FMLPostInitializationEvent e) {
    }

    @SubscribeEvent
    public void gloveMiningSpeed(net.minecraftforge.event.entity.player.PlayerEvent.BreakSpeed event) {
        if (event.getEntityPlayer() == null) return;

        ItemStack glove = ItemStack.EMPTY;

        IBaublesItemHandler baubles = BaublesApi.getBaublesHandler(event.getEntityPlayer());
        for(int i = 0; i < baubles.getSlots(); i++) {
            ItemStack slot = baubles.getStackInSlot(i);
            if (slot != null && slot != ItemStack.EMPTY && slot.getItem() instanceof ItemGloves){
                glove = slot;
                break;
            }
        }

        if(glove != null) {

        }
    }
}
