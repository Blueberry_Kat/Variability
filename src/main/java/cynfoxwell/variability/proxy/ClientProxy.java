package cynfoxwell.variability.proxy;

import cynfoxwell.variability.client.*;
import cynfoxwell.variability.item.ModItems;
import cynfoxwell.variability.item.dyable.IDyable;
import cynfoxwell.variability.util.ColorList;
import cynfoxwell.variability.util.ColorUtil;
import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.color.IItemColor;
import net.minecraft.client.renderer.entity.RenderPlayer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.ColorHandlerEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPostInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

import java.util.Map;


@Mod.EventBusSubscriber(Side.CLIENT)
public class ClientProxy extends CommonProxy {
    @Override
    public void preInit(FMLPreInitializationEvent e) {
        super.preInit(e);
        //ClientRegistry.bindTileEntitySpecialRenderer(TileEntityCompressedChest.class, new RendererCompressedChest());
    }

    @Override
    public void init(FMLInitializationEvent e){
        super.init(e);
    }

    @Override
    public void postInit(FMLPostInitializationEvent e){
        super.postInit(e);

        Map<String, RenderPlayer> renders = Minecraft.getMinecraft().getRenderManager().getSkinMap();
        for (Map.Entry<String, RenderPlayer> en : renders.entrySet()) {
            en.getValue().addLayer(new LayerProgrammingSocks(en.getValue()));
            en.getValue().addLayer(new LayerScarf(en.getValue()));

            en.getValue().addLayer(new LayerBaubleBag());
        }

        RenderPlayer render;
        render = renders.get("default");
        render.addLayer(new LayerGloves(render));
        render = renders.get("slim");
        render.addLayer(new LayerGlovesSlim(render));
    }

    @SubscribeEvent
    public static void registerItemHandlers(ColorHandlerEvent.Item event)
    {
        //Dyable Items
        event.getItemColors().registerItemColorHandler((stack, tintIndex) -> tintIndex > 0 ? -1 : ((IDyable)stack.getItem()).getColor(stack),
                ModItems.scarf,
                ModItems.preciseDye,
                ModItems.gloves
        );

        //Programming Socks
        event.getItemColors().registerItemColorHandler(new IItemColor() {
            @Override
            public int colorMultiplier(ItemStack stack, int tintIndex) {
                switch (tintIndex){
                    case 0:
                        return ((IDyable)stack.getItem()).getColor(stack);
                    case 1:
                        return stack.getItem().getDamage(stack) < 16 ? ColorList.values()[stack.getItem().getDamage(stack)].color : (stack.getItem().getDamage(stack) == 16 ? ColorUtil.RainbowEffect(2,1,1) : ColorUtil.RainbowEffect(2,0.5F,1));
                    default:
                        return -1;
                }
            }
        },ModItems.programmingSocks);
    }
}
