package cynfoxwell.variability.item;

import cynfoxwell.variability.client.font.PastelNameFont;
import cynfoxwell.variability.item.base.ItemBase;
import net.minecraft.client.gui.FontRenderer;
import net.minecraft.item.ItemStack;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;

public class ItemPastelDye extends ItemBase {
    public ItemPastelDye() {
        super("pastel_dye");
    }

    @SideOnly(Side.CLIENT)
    @Nullable
    @Override
    public FontRenderer getFontRenderer(ItemStack stack) {
        return PastelNameFont.get();
    }
}
