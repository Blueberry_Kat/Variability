package cynfoxwell.variability.item.base;

import cynfoxwell.variability.Variability;
import net.minecraft.block.Block;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemSeeds;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBaseSeed extends ItemSeeds {
    public ItemBaseSeed(String name, Block crops, Block soil) {
        super(crops, soil);

        setCreativeTab(Variability.tabVariability);
        setRegistryName(new ResourceLocation(Variability.MODID, name));
        setUnlocalizedName(Variability.MODID+"."+name);
    }

    @SideOnly(Side.CLIENT)
    public void initModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }
}
