package cynfoxwell.variability.item.base;

import cynfoxwell.variability.util.TextUtil;
import net.minecraft.block.Block;
import net.minecraft.block.BlockFarmland;
import net.minecraft.block.IGrowable;
import net.minecraft.block.state.IBlockState;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.Blocks;
import net.minecraft.item.EnumAction;
import net.minecraft.item.ItemStack;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.EnumHand;
import net.minecraft.util.EnumParticleTypes;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.world.World;
import net.minecraftforge.event.ForgeEventFactory;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class ItemBaseWateringCan extends ItemBase {
    private int range = 0;
    private int chance = 1;

    private boolean canWater = false;
    private boolean showParticles = false;
    private boolean forceActive = false;

    private long ticks;

    private List<String> ranges = new ArrayList<>();

    public ItemBaseWateringCan(String name, int range, int chance) {
        super(name);
        this.range = range;
        this.chance = chance;

        ranges.add("1x1");
        ranges.add("3x3");
        ranges.add("5x5");
        ranges.add("7x7");
        ranges.add("9x9");
    }

    public void onUpdate(ItemStack stack, World worldIn, Entity entityIn, int itemSlot, boolean isSelected)
    {
        ticks++;

        if(ticks % 4 == 0)
        {
            showParticles = true;
            canWater = true;
        }

        if(forceActive && entityIn instanceof EntityPlayer)
        {
            EntityPlayer player = (EntityPlayer)entityIn;

            if(!isSelected)
            {
                ItemStack offhand = player.getHeldItem(EnumHand.OFF_HAND);

                if(offhand.getItem() instanceof ItemBaseWateringCan)
                    forceActive = false;
            }

            RayTraceResult raytrace = rayTrace(worldIn, player, false);
            if( raytrace != null && raytrace.typeOfHit == RayTraceResult.Type.BLOCK)
            {
                attemptWaterParticles(worldIn, raytrace.getBlockPos());
                attemptWater(worldIn, raytrace.getBlockPos());
            }
        }
    }

    @Override
    public EnumAction getItemUseAction(ItemStack stack)
    {
        return EnumAction.NONE;
    }

    int clicks = 0;

    @Override
    public EnumActionResult onItemUse(EntityPlayer player, World world, BlockPos pos, EnumHand hand, EnumFacing facing, float hitX, float hitY, float hitZ)
    {
        if(!player.canPlayerEdit(pos.offset(facing), facing, player.getHeldItem(hand)))
        {
            return EnumActionResult.FAIL;
        }

        attemptWaterParticles(world, pos);
        return attemptWater(world, pos);
    }

    private static int randInt(int min, int max){

        Random rand = new Random();

        int randomNum = rand.nextInt(max - min + 1) + min;

        return randomNum;
    }

    private EnumActionResult attemptWater(World world, BlockPos pos)
    {
        if(!world.isRemote && canWater)
        {
            canWater = false;
            int rchance = randInt(1, 100);
            if(rchance <= chance)
            {
                for(int xAxis = -range; xAxis <= range; xAxis++)
                {
                    for(int zAxis = -range; zAxis <= range; zAxis++)
                    {
                        for(int yAxis = -range; yAxis <= range; yAxis++)
                        {
                            Block checkBlock = world.getBlockState(pos.add(xAxis, yAxis, zAxis)).getBlock();

                            if(checkBlock instanceof IGrowable || checkBlock == Blocks.MYCELIUM
                                    || checkBlock == Blocks.CACTUS || checkBlock == Blocks.REEDS
                                    || checkBlock == Blocks.CHORUS_FLOWER)
                            {
                                world.scheduleBlockUpdate(pos.add(xAxis, yAxis, zAxis), checkBlock, 0, 1);
                            }
                        }
                    }
                }
                return EnumActionResult.FAIL;
            }
        }
        return EnumActionResult.FAIL;
    }

    private void attemptWaterParticles(World world, BlockPos pos)
    {
        if(this.showParticles)
        {
            this.showParticles = false;

            Random rand = new Random();
            for(int x = -range; x <= range; x++)
            {
                for(int z = -range; z <= range; z++)
                {
                    double d0 = pos.add(x, 0, z).getX() + rand.nextFloat();
                    double d1 = pos.add(x, 0, z).getY() + 1.0D;
                    double d2 = pos.add(x, 0, z).getZ() + rand.nextFloat();

                    IBlockState checkSolidState = world.getBlockState(pos);
                    Block checkSolid = checkSolidState.getBlock();
                    if((checkSolid.isFullCube(checkSolidState)) || ((checkSolid instanceof BlockFarmland)))
                    {
                        d1 += 1.0D;
                    }

                    world.spawnParticle(EnumParticleTypes.WATER_DROP, d0, d1, d2, 0.0D, 0.0D, 0.0D, new int[5]);
                }
            }
        }
    }

    public static boolean applyBonemeal(ItemStack stack, World worldIn, BlockPos target, EntityPlayer player, EnumHand hand)
    {

        IBlockState iblockstate = worldIn.getBlockState(target);

        int hook = ForgeEventFactory.onApplyBonemeal(player, worldIn, target, iblockstate, stack, hand);
        if(hook != 0)
            return hook > 0;

        if((iblockstate.getBlock() instanceof IGrowable && iblockstate.getBlock() != Blocks.GRASS))
        {
            IGrowable igrowable = (IGrowable)iblockstate.getBlock();

            if(igrowable.canGrow(worldIn, target, iblockstate, worldIn.isRemote))
            {
                if(!worldIn.isRemote)
                {
                    if(igrowable.canUseBonemeal(worldIn, worldIn.rand, target, iblockstate))
                    {
                        igrowable.grow(worldIn, worldIn.rand, target, iblockstate);
                    }

                    stack.setCount(stack.getCount() - 1);
                }

                return true;
            }
        }
        return false;
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn){
        tooltip.add(TextUtil.formatColors(I18n.format("variability.tooltip.can.1",ranges.toArray()[this.range])));
        tooltip.add(TextUtil.formatColors(I18n.format("variability.tooltip.can.2",this.chance+"%")));
    }
}
