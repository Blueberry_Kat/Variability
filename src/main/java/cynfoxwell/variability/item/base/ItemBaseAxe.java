package cynfoxwell.variability.item.base;

import cynfoxwell.variability.Variability;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.item.ItemAxe;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class ItemBaseAxe extends ItemAxe {
    public ItemBaseAxe(String name, ToolMaterial material) {
        super(material, 8F, -3.1F);
        setCreativeTab(Variability.tabVariability);
        setRegistryName(new ResourceLocation(Variability.MODID, name));
        setUnlocalizedName(Variability.MODID+"."+name);
    }

    @SideOnly(Side.CLIENT)
    public void initModel() {
        ModelLoader.setCustomModelResourceLocation(this, 0, new ModelResourceLocation(getRegistryName(), "inventory"));
    }
}
