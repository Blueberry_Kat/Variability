package cynfoxwell.variability.item.dyable;

import net.minecraft.item.ItemStack;

public interface IDyable {
    //Dummy interface for unified code
    void setColor(ItemStack stack, int color);

    void removeColor(ItemStack stack);

    int getColor(ItemStack stack);

    boolean hasColor(ItemStack stack);
}
