package cynfoxwell.variability.item;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.inventory.GuiHandler;
import cynfoxwell.variability.item.dyable.ItemDyable;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class ItemPreciseDye extends ItemDyable {
    public ItemPreciseDye() {
        super("precise_dye");
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn){
        tooltip.add(I18n.format("variability.tooltip.precise_dye"));
    }

    @Nonnull
    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, @Nonnull EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);

        player.openGui(Variability.INSTANCE, GuiHandler.GuiTypes.PRECISE_DYE.ordinal(), world, (int)player.posX, (int)player.posY, (int)player.posZ);
        return ActionResult.newResult(EnumActionResult.SUCCESS, stack);
    }
}
