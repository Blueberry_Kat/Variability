package cynfoxwell.variability.item;

import cynfoxwell.variability.item.base.ItemBaseFood;
import net.minecraft.item.ItemStack;

public class ItemBacon extends ItemBaseFood {
    public ItemBacon() {
        super("bacon", 8, 0.8F, true);
    }

    @Override
    public boolean isBeaconPayment(ItemStack stack)
    {
        return true;
    }
}
