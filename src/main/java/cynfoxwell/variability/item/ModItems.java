package cynfoxwell.variability.item;

import cynfoxwell.variability.Variability;
import cynfoxwell.variability.block.ModBlocks;
import cynfoxwell.variability.config.ConfigHandler;
import cynfoxwell.variability.item.base.ItemBase;
import cynfoxwell.variability.item.base.ItemBasePaxel;
import cynfoxwell.variability.item.base.ItemBaseSeed;
import cynfoxwell.variability.item.base.ItemBaseWateringCan;
import cynfoxwell.variability.item.bauble.ItemBaubleBag;
import cynfoxwell.variability.item.bauble.ItemGloves;
import cynfoxwell.variability.item.bauble.ItemProgrammingSocks;
import cynfoxwell.variability.item.bauble.ItemScarf;
import cynfoxwell.variability.item.compat.ItemPaxelClay;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.oredict.OreDictionary;
import net.minecraftforge.registries.IForgeRegistry;
import org.apache.logging.log4j.Level;

import java.io.File;

@GameRegistry.ObjectHolder(Variability.MODID)
public class ModItems {
    @GameRegistry.ObjectHolder("bauble_bag")
    public static ItemBaubleBag baubleBag = new ItemBaubleBag();

    //Cosmetics
    @GameRegistry.ObjectHolder("chromatic_dye")
    public static ItemChromaticDye chromaticDye = new ItemChromaticDye();
    @GameRegistry.ObjectHolder("pastel_dye")
    public static ItemPastelDye pastelDye = new ItemPastelDye();
    @GameRegistry.ObjectHolder("precise_dye")
    public static ItemPreciseDye preciseDye = new ItemPreciseDye();
    @GameRegistry.ObjectHolder("scarf")
    public static ItemScarf scarf = new ItemScarf();
    @GameRegistry.ObjectHolder("thighhighs")
    public static ItemProgrammingSocks programmingSocks = new ItemProgrammingSocks();
    @GameRegistry.ObjectHolder("gloves")
    public static ItemGloves gloves = new ItemGloves();
    @GameRegistry.ObjectHolder("cloth")
    public static ItemBase cloth = new ItemBase("cloth");

    //Paxels
    //Vanilla Paxels
    @GameRegistry.ObjectHolder("wood_paxel")
    public static ItemBasePaxel woodPaxel = new ItemBasePaxel("wood_paxel", Item.ToolMaterial.WOOD);
    @GameRegistry.ObjectHolder("stone_paxel")
    public static ItemBasePaxel stonePaxel = new ItemBasePaxel("stone_paxel", Item.ToolMaterial.STONE);
    @GameRegistry.ObjectHolder("iron_paxel")
    public static ItemBasePaxel ironPaxel = new ItemBasePaxel("iron_paxel", Item.ToolMaterial.IRON);
    @GameRegistry.ObjectHolder("gold_paxel")
    public static ItemBasePaxel goldPaxel = new ItemBasePaxel("gold_paxel", Item.ToolMaterial.GOLD);
    @GameRegistry.ObjectHolder("diamond_paxel")
    public static ItemBasePaxel diamondPaxel = new ItemBasePaxel("diamond_paxel", Item.ToolMaterial.DIAMOND);

    //Iridescent Paxels
    @GameRegistry.ObjectHolder("cobblestone_paxel")
    public static ItemBasePaxel cobblePaxel = new ItemBasePaxel("cobblestone_paxel", Item.ToolMaterial.STONE);
    @GameRegistry.ObjectHolder("granite_paxel")
    public static ItemBasePaxel granitePaxel = new ItemBasePaxel("granite_paxel", Item.ToolMaterial.STONE);
    @GameRegistry.ObjectHolder("andesite_paxel")
    public static ItemBasePaxel andesitePaxel = new ItemBasePaxel("andesite_paxel", Item.ToolMaterial.STONE);
    @GameRegistry.ObjectHolder("diorite_paxel")
    public static ItemBasePaxel dioritePaxel = new ItemBasePaxel("diorite_paxel", Item.ToolMaterial.STONE);
    @GameRegistry.ObjectHolder("flint_paxel")
    public static ItemBasePaxel flintPaxel = new ItemBasePaxel("flint_paxel", Item.ToolMaterial.STONE);

    //BCT Paxel
    @GameRegistry.ObjectHolder("clay_paxel")
    public static ItemPaxelClay clayPaxel = new ItemPaxelClay();

    //Agriculture
    @GameRegistry.ObjectHolder("seeds_flax")
    public static ItemBaseSeed flaxSeeds = new ItemBaseSeed("seeds_flax", ModBlocks.cropFlax, Blocks.FARMLAND);
    @GameRegistry.ObjectHolder("watering_can_wood")
    public static ItemBaseWateringCan canWood = new ItemBaseWateringCan("watering_can_wood",0,25);
    @GameRegistry.ObjectHolder("watering_can_stone")
    public static ItemBaseWateringCan canStone = new ItemBaseWateringCan("watering_can_stone",1,25);
    @GameRegistry.ObjectHolder("watering_can_iron")
    public static ItemBaseWateringCan canIron = new ItemBaseWateringCan("watering_can_iron",2,50);
    @GameRegistry.ObjectHolder("watering_can_gold")
    public static ItemBaseWateringCan canGold = new ItemBaseWateringCan("watering_can_gold",2,75);
    @GameRegistry.ObjectHolder("watering_can_diamond")
    public static ItemBaseWateringCan canDiamond = new ItemBaseWateringCan("watering_can_diamond",3,75);
    @GameRegistry.ObjectHolder("watering_can_emerald")
    public static ItemBaseWateringCan canEmerald = new ItemBaseWateringCan("watering_can_emerald",4,100);

    //Misc
    @GameRegistry.ObjectHolder("emerald_chip")
    public static ItemBase emeraldChip = new ItemBase("emerald_chip");
    @GameRegistry.ObjectHolder("emerald_chunk")
    public static ItemBase emeraldChunk = new ItemBase("emerald_chunk");

    static {
        if(ConfigHandler.enableAgriculture){
            if(ConfigHandler.agrFlaxSeedWeight > 0)
                MinecraftForge.addGrassSeed(new ItemStack(flaxSeeds), ConfigHandler.agrFlaxSeedWeight);
        }
    }

    @Mod.EventBusSubscriber(modid = Variability.MODID)
    public static class Registration {
        @SubscribeEvent
        public static void registerItems(RegistryEvent.Register<Item> event) {
            IForgeRegistry<Item> itemRegistry = event.getRegistry();

            if(ConfigHandler.enableBBag) {
                Variability.LOGGER.log(Level.INFO,"Registering Bauble Bag Module");

                itemRegistry.register(baubleBag);
            }

            if(ConfigHandler.enableCosmetics) {
                Variability.LOGGER.log(Level.INFO,"Registering Cosmetics Module");

                itemRegistry.register(chromaticDye);
                itemRegistry.register(pastelDye);
                itemRegistry.register(preciseDye);
                itemRegistry.register(scarf);
                itemRegistry.register(programmingSocks);
                itemRegistry.register(gloves);
                itemRegistry.register(cloth);
            }

            if(ConfigHandler.enablePaxels) {
                Variability.LOGGER.log(Level.INFO,"Registering Paxels Module");

                itemRegistry.register(woodPaxel);
                itemRegistry.register(stonePaxel);
                itemRegistry.register(ironPaxel);
                itemRegistry.register(goldPaxel);
                itemRegistry.register(diamondPaxel);

                if(Loader.isModLoaded("iridescent") || ConfigHandler.paxelIridescent) {
                    Configuration c = new Configuration(new File("config/iridescent.cfg"));

                    if (c.getBoolean("stoned", "modules", true, "") || ConfigHandler.paxelIridescent) {
                        Variability.LOGGER.log(Level.INFO, "[Paxels] Registering Iridescent mod support. (forced: " + ConfigHandler.paxelIridescent + ")");

                        itemRegistry.register(cobblePaxel);
                        itemRegistry.register(granitePaxel);
                        itemRegistry.register(andesitePaxel);
                        itemRegistry.register(dioritePaxel);
                        itemRegistry.register(flintPaxel);
                    }
                }else{
                    Variability.LOGGER.log(Level.INFO,"[Paxels] Skipped Iridescent support. Feel free to enable forced mode if you want them.");
                }

                if(Loader.isModLoaded("balancedclaytools") || ConfigHandler.paxelBCT) {
                    Variability.LOGGER.log(Level.INFO,"[Paxels] Registering Balanced Clay Tools mod support. (forced: "+ConfigHandler.paxelBCT+")");

                    itemRegistry.register(clayPaxel);
                }else{
                    Variability.LOGGER.log(Level.INFO,"[Paxels] Skipped Balanced Clay Tools support. Feel free to enable forced mode if you want them.");
                }
            }

            if(ConfigHandler.enableAgriculture) {
                Variability.LOGGER.log(Level.INFO,"Registering Agricultural Module Items");

                itemRegistry.register(flaxSeeds);

                itemRegistry.register(canWood);
                itemRegistry.register(canStone);
                itemRegistry.register(canIron);
                itemRegistry.register(canGold);
                itemRegistry.register(canDiamond);
                itemRegistry.register(canEmerald);

                OreDictionary.registerOre("seedFlax", new ItemStack(flaxSeeds));
            }

            if(ConfigHandler.miscEmeraldPieces){
                Variability.LOGGER.log(Level.INFO,"Registering Emerald Pieces");

                //itemRegistry.register(emeraldChip);
                //itemRegistry.register(emeraldChunk);
            }
        }

        @SubscribeEvent
        public static void registerModels (ModelRegistryEvent event) {
            if(ConfigHandler.enableBBag)
                baubleBag.initModel();

            if(ConfigHandler.enableCosmetics) {
                chromaticDye.initModel();
                pastelDye.initModel();
                preciseDye.initModel();
                scarf.initModel();
                programmingSocks.initModel();
                gloves.initModel();
                cloth.initModel();
            }

            if(ConfigHandler.enablePaxels) {
                woodPaxel.initModel();
                stonePaxel.initModel();
                ironPaxel.initModel();
                goldPaxel.initModel();
                diamondPaxel.initModel();

                if(Loader.isModLoaded("iridescent") || ConfigHandler.paxelIridescent) {
                    cobblePaxel.initModel();
                    granitePaxel.initModel();
                    andesitePaxel.initModel();
                    dioritePaxel.initModel();
                    flintPaxel.initModel();
                }

                if(Loader.isModLoaded("balancedclaytools") || ConfigHandler.paxelBCT)
                    clayPaxel.initModel();
            }

            if(ConfigHandler.enableAgriculture) {
                flaxSeeds.initModel();
                canWood.initModel();
                canStone.initModel();
                canIron.initModel();
                canGold.initModel();
                canDiamond.initModel();
                canEmerald.initModel();
            }

            if(ConfigHandler.miscEmeraldPieces){
                //emeraldChip.initModel();
                //emeraldChunk.initModel();
            }
        }
    }
}
