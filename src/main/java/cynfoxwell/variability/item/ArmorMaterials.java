package cynfoxwell.variability.item;

import cynfoxwell.variability.Variability;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemArmor;
import net.minecraft.util.SoundEvent;
import net.minecraftforge.common.util.EnumHelper;

public enum ArmorMaterials {
    COSMETIC("COSMETIC", "null", 0, new int[]{0,0,0,0}, 0, SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, 0F),
    AMETHYST("AMETHYST", "amethyst", 20, new int[]{4, 7, 9, 4}, 15, SoundEvents.ITEM_ARMOR_EQUIP_DIAMOND, 2.5F);

    public ItemArmor.ArmorMaterial material;

    ArmorMaterials(String name, String texture, int damageFactor, int[] armorLevels, int enchantability, SoundEvent equipSound, float toughness) {
        this.material = EnumHelper.addArmorMaterial(name,Variability.MODID+":"+texture,damageFactor,armorLevels,enchantability,equipSound,toughness);
    }

    public ItemArmor.ArmorMaterial getMaterial() {
        return material;
    }
}