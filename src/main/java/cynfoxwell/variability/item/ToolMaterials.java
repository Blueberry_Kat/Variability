package cynfoxwell.variability.item;


import cynfoxwell.variability.Variability;
import net.minecraft.item.Item;
import net.minecraftforge.common.util.EnumHelper;

public enum ToolMaterials {
    EMERALD("emerald",3,1920,9.0F,4.0F,15),
    OBSIDIAN("obsidian",3,2816,4.0F,2.0F,15);

    public Item.ToolMaterial material;

    ToolMaterials(String name, int level, int uses, float speed, float damage, int enchantability) {
        this.material = EnumHelper.addToolMaterial(Variability.MODID+"."+name,level,uses,speed,damage,enchantability);
    }

    public Item.ToolMaterial getMaterial() {
        return material;
    }
}
