package cynfoxwell.variability.item.bauble;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.IBauble;
import baubles.api.cap.IBaublesItemHandler;
import cynfoxwell.variability.Variability;
import cynfoxwell.variability.inventory.GuiHandler;
import cynfoxwell.variability.item.base.ItemBase;
import cynfoxwell.variability.tile.TileEntityBaubleBag;
import cynfoxwell.variability.util.TextUtil;
import net.minecraft.client.resources.I18n;
import net.minecraft.client.util.ITooltipFlag;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.items.IItemHandler;
import net.minecraftforge.items.IItemHandlerModifiable;
import net.minecraftforge.items.ItemHandlerHelper;
import net.minecraftforge.items.ItemStackHandler;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;

public class ItemBaubleBag extends ItemBase implements IBauble {
    public ItemBaubleBag() {
        super("bauble_bag");
        setMaxStackSize(1);
    }

    @Override
    public BaubleType getBaubleType(ItemStack itemstack) {
        return BaubleType.BELT;
    }

    @Override
    public void onWornTick(ItemStack stack, EntityLivingBase player) {
        NBTTagCompound compound = stack.getTagCompound();
        if(compound == null){
            return;
        }

        ItemStackHandler inv = new ItemStackHandler(18);
        loadSlotsFromNBT(inv,stack);

        for(int i = 0; i < inv.getSlots(); i++) {
            ItemStack slot = inv.getStackInSlot(i);
            if(slot != ItemStack.EMPTY && slot.getItem() instanceof IBauble){
                IBauble bauble = (IBauble) slot.getItem();
                bauble.onWornTick(slot,player);
            }
        }
    }

    @Override
    @SideOnly(Side.CLIENT)
    public void addInformation(ItemStack stack, @Nullable World worldIn, List<String> tooltip, ITooltipFlag flagIn){
        tooltip.add(TextUtil.formatColors(I18n.format("variability.tooltip.bauble_bag.1")));
        tooltip.add(TextUtil.formatColors(I18n.format("variability.tooltip.bauble_bag.2")));
    }

    public static void loadSlotsFromNBT(IItemHandlerModifiable slots, ItemStack stack){
        NBTTagCompound compound = stack.getTagCompound();
        if(compound != null){
            TileEntityBaubleBag.loadSlots(slots, compound);
        }
    }

    public static void writeSlotsToNBT(IItemHandler slots, ItemStack stack){
        NBTTagCompound compound = stack.getTagCompound();
        if(compound == null){
            compound = new NBTTagCompound();
        }
        TileEntityBaubleBag.saveSlots(slots, compound);
        stack.setTagCompound(compound);
    }

    @Nonnull
    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, @Nonnull EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);

        if(!world.isRemote && player.isSneaking() && hand == EnumHand.MAIN_HAND){
            player.openGui(Variability.INSTANCE, GuiHandler.GuiTypes.BAUBLE_BAG.ordinal(), world, (int)player.posX, (int)player.posY, (int)player.posZ);
        }else {
            ItemStack toEquip = stack.copy();
            toEquip.setCount(1);

            if (world.isRemote)
                return ActionResult.newResult(EnumActionResult.SUCCESS, stack);

            IBaublesItemHandler baubles = BaublesApi.getBaublesHandler(player);
            for (int i = 0; i < baubles.getSlots(); i++) {
                if (baubles.isItemValidForSlot(i, toEquip, player)) {
                    ItemStack stackInSlot = baubles.getStackInSlot(i);
                    if (stackInSlot.isEmpty() || ((IBauble) stackInSlot.getItem()).canUnequip(stackInSlot, player)) {
                        baubles.setStackInSlot(i, ItemStack.EMPTY);

                        baubles.setStackInSlot(i, toEquip);
                        ((IBauble) toEquip.getItem()).onEquipped(toEquip, player);

                        stack.shrink(1);

                        if (!stackInSlot.isEmpty()) {
                            ((IBauble) stackInSlot.getItem()).onUnequipped(stackInSlot, player);

                            if (stack.isEmpty()) {
                                return ActionResult.newResult(EnumActionResult.SUCCESS, stackInSlot);
                            } else {
                                ItemHandlerHelper.giveItemToPlayer(player, stackInSlot);
                            }
                        }

                        return ActionResult.newResult(EnumActionResult.SUCCESS, stack);
                    }
                }
            }
        }

        return ActionResult.newResult(EnumActionResult.PASS, stack);
    }

    @Override
    public void onEquipped(ItemStack itemstack, EntityLivingBase player) {
        player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, .75F, 1.9f);
    }

    @Override
    public void onUnequipped(ItemStack itemstack, EntityLivingBase player) {
        player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_LEATHER, .75F, 2f);
    }
}
