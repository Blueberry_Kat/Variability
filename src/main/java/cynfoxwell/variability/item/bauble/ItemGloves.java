package cynfoxwell.variability.item.bauble;

import baubles.api.BaubleType;
import baubles.api.BaublesApi;
import baubles.api.IBauble;
import baubles.api.cap.IBaublesItemHandler;
import cynfoxwell.variability.item.dyable.ItemDyable;
import cynfoxwell.variability.util.ColorUtil;
import net.minecraft.enchantment.EnumEnchantmentType;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.init.SoundEvents;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.util.ActionResult;
import net.minecraft.util.EnumActionResult;
import net.minecraft.util.EnumHand;
import net.minecraft.world.World;
import net.minecraftforge.items.ItemHandlerHelper;

import javax.annotation.Nonnull;

public class ItemGloves extends ItemDyable implements IBauble {
    public ItemGloves() {
        super("gloves");
        setMaxStackSize(1);
    }

    @Override
    public BaubleType getBaubleType(ItemStack itemstack) {
        return BaubleType.TRINKET;
    }

    @Nonnull
    @Override
    public ActionResult<ItemStack> onItemRightClick(World world, EntityPlayer player, @Nonnull EnumHand hand) {
        ItemStack stack = player.getHeldItem(hand);

        ItemStack toEquip = stack.copy();
        toEquip.setCount(1);

        if(world.isRemote)
            return ActionResult.newResult(EnumActionResult.SUCCESS, stack);

        IBaublesItemHandler baubles = BaublesApi.getBaublesHandler(player);
        for(int i = 0; i < baubles.getSlots(); i++) {
            if(baubles.isItemValidForSlot(i, toEquip, player)) {
                ItemStack stackInSlot = baubles.getStackInSlot(i);
                if(stackInSlot.isEmpty() || ((IBauble) stackInSlot.getItem()).canUnequip(stackInSlot, player)) {
                    baubles.setStackInSlot(i, ItemStack.EMPTY);

                    baubles.setStackInSlot(i, toEquip);
                    ((IBauble) toEquip.getItem()).onEquipped(toEquip, player);

                    stack.shrink(1);

                    player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_GENERIC,1F,1F);

                    if(!stackInSlot.isEmpty()) {
                        ((IBauble) stackInSlot.getItem()).onUnequipped(stackInSlot, player);

                        if(stack.isEmpty()) {
                            return ActionResult.newResult(EnumActionResult.SUCCESS, stackInSlot);
                        } else {
                            ItemHandlerHelper.giveItemToPlayer(player, stackInSlot);
                        }
                    }

                    return ActionResult.newResult(EnumActionResult.SUCCESS, stack);
                }
            }
        }

        return ActionResult.newResult(EnumActionResult.PASS, stack);
    }

    @Override
    public void onEquipped(ItemStack itemstack, EntityLivingBase player) {
        player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, .75F, 1.9f);
    }

    @Override
    public void onUnequipped(ItemStack itemstack, EntityLivingBase player) {
        player.playSound(SoundEvents.ITEM_ARMOR_EQUIP_GENERIC, .75F, 2f);
    }

    @Override
    public boolean canApplyAtEnchantingTable(ItemStack stack, net.minecraft.enchantment.Enchantment enchantment)
    {
        return enchantment.type.equals(EnumEnchantmentType.DIGGER);
    }

    @Override
    public int getColor(ItemStack stack)
    {
        NBTTagCompound nbttagcompound = stack.getTagCompound();

        if (nbttagcompound != null)
        {
            NBTTagCompound nbttagcompound1 = nbttagcompound.getCompoundTag("display");

            if (nbttagcompound1 != null && nbttagcompound1.hasKey("color", 3))
            {
                return nbttagcompound1.getInteger("color");
            }

            if(nbttagcompound1 != null && nbttagcompound.hasKey("isChromatic")){
                return ColorUtil.RainbowEffect(2,1,1);
            }

            if(nbttagcompound1 != null && nbttagcompound.hasKey("isPastel")){
                return ColorUtil.RainbowEffect(2,0.5F,1);
            }
        }

        return 10511680;
    }
}
